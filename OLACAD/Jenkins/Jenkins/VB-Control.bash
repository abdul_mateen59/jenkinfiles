#!/bin/bash

# start VM
echo "start VM"
/c/Program\ Files/Oracle/VirtualBox/VBoxManage.exe startvm   "Win7de64_ACAD2015"
#/c/Program\ Files/Oracle/VirtualBox/VBoxManage.exe startv {914ad93c-6981-4887-91ae-e4b324e445b1}
sleep 5

#copy installation files to guest
echo "copy installation files"
/c/Program\ Files/Oracle/VirtualBox/VBoxManage.exe --nologo guestcontrol "Win7de64_ACAD2015" --username "Admin" --password "ac4tion!" copyto --target-directory "d:\OLAcad" "d:\AcadUnitTest\OlacadClientInstall_AutoCAD2015de_full.msi"

# must already exist in guest. ACAD will REMOVE FROM SEARCH PATH if this directory is not existing
#/c/Program\ Files/Oracle/VirtualBox/VBoxManage.exe --nologo guestcontrol "Win7de64_ACAD2015" --username "Admin" --password "ac4tion!" mkdir "C:\ProgramData\Autodesk\ApplicationPlugins\AutocadUnitTestLoader.bundle"
/c/Program\ Files/Oracle/VirtualBox/VBoxManage.exe --nologo guestcontrol "Win7de64_ACAD2015" --username "Admin" --password "ac4tion!" copyto --target-directory "C:\ProgramData\Autodesk\ApplicationPlugins\AutocadUnitTestLoader.bundle" "D:\AcadUnitTest\AutocadUnitTestLoader.bundle\autocad.lsp"
/c/Program\ Files/Oracle/VirtualBox/VBoxManage.exe --nologo guestcontrol "Win7de64_ACAD2015" --username "Admin" --password "ac4tion!" copyto --target-directory "C:\ProgramData\Autodesk\ApplicationPlugins\AutocadUnitTestLoader.bundle" "D:\AcadUnitTest\AutocadUnitTestLoader.bundle\PackageContents.xml"


/c/Program\ Files/Oracle/VirtualBox/VBoxManage.exe --nologo guestcontrol "Win7de64_ACAD2015" --username "Admin" --password "ac4tion!" mkdir "D:\OLAcad\AutocadUnitTest.bundle"
/c/Program\ Files/Oracle/VirtualBox/VBoxManage.exe --nologo guestcontrol "Win7de64_ACAD2015" --username "Admin" --password "ac4tion!" copyto --target-directory "D:\OLAcad\AutocadUnitTest.bundle" "D:\AcadUnitTest\AutocadUnitTest.bundle\AcadUnitTest.arx"
/c/Program\ Files/Oracle/VirtualBox/VBoxManage.exe --nologo guestcontrol "Win7de64_ACAD2015" --username "Admin" --password "ac4tion!" copyto --target-directory "D:\OLAcad\AutocadUnitTest.bundle" "D:\AcadUnitTest\AutocadUnitTest.bundle\PackageContents.xml"

# OLAcad msi installieren:
echo "install OLAcad"
/c/Program\ Files/Oracle/VirtualBox/VBoxManage.exe --nologo guestcontrol "Win7de64_ACAD2015" run --username "Admin" --password "ac4tion!" --wait-stdout --exe "C:\Windows\System32\msiexec.exe" -- "msiexec.exe" -package "D:\OLAcad\OlacadClientInstall_AutoCAD2015de_full.msi" -passive -qr -log "d:\OLACAD\install_msi_remote.log"

# if DEBUG Version is loaded also copy wupiEngineNet.dll
echo "copy WupiEngineNet.dll for testing debug arx"
/c/Program\ Files/Oracle/VirtualBox/VBoxManage.exe --nologo guestcontrol "Win7de64_ACAD2015" copyto --username "Admin" --password "ac4tion!" --target-directory "C:\ProgramData\Autodesk\ApplicationPlugins\OLACAD_Loader.bundle" "D:\AcadUnitTest\WupiEngineNet.dll"

# set environment
export JUNIT_OUT_DIR="D:\OLAcad"
# start ACAD and kill after 30sec (should have loeded and executed test 'till then)
echo "start ACAD and terminate after 30sec"
/c/Program\ Files/Oracle/VirtualBox/VBoxManage.exe --nologo guestcontrol "Win7de64_ACAD2015" --username "Admin" --password "ac4tion!" run --exe "C:\Program Files\Autodesk\AutoCAD 2015\acad.exe" --putenv JUNIT_OUT_DIR="D:\OLAcad" --timeout 30000 -- "acad.exe" -product ACAD -language "de-DE"
#/c/Program\ Files/Oracle/VirtualBox/VBoxManage.exe --nologo guestcontrol "Win7de64_ACAD2015" --username "Admin" --password "ac4tion!" run --exe "C:\Program Files\Autodesk\AutoCAD 2015\acad.exe" --putenv JUNIT_OUT_DIR="D:\OLAcad" -- "acad.exe" -product ACAD -language "de-DE"

# save junit_result file
if [ -f D:\AcadUnitTest\junit_result.xml ] 
then
	echo "delete old result file(s)"
	rm  D:\AcadUnitTest\junit_result.xml
else
	echo "no file to delete"
fi

echo "save junit_result file(s)"
/c/Program\ Files/Oracle/VirtualBox/VBoxManage.exe --nologo guestcontrol "Win7de64_ACAD2015" --username "Admin" --password "ac4tion!" copyfrom --verbose --target-directory "D:\AcadUnitTest\junit_result.xml" "D:\OLAcad\junit_result.xml"
#/c/Program\ Files/Oracle/VirtualBox/VBoxManage.exe --nologo guestcontrol "Win7de64_ACAD2015" --username "Admin" --password "ac4tion!" copyfrom --target-directory "D:\UpdateServer\junit_result.xml" "C:\Users\Admin\Documents\junit_result.xml"
#/c/Program\ Files/Oracle/VirtualBox/VBoxManage.exe --nologo guestcontrol "Win7de64_ACAD2015" --username "Admin" --password "ac4tion!" copyfrom --verbose --target-directory "D:\AcadUnitTest\junit_result.xml" "C:\Windows\System32\junit_result.xml"
#/c/Program\ Files/Oracle/VirtualBox/VBoxManage.exe --nologo guestcontrol "Win7de64_ACAD2015" --username "Admin" --password "ac4tion!" copyfrom --verbose --target-directory "D:\AcadUnitTest\junit_result.xml" "C:\Users\Admin\AppData\Local\Temp\junit_result.xml"

# powerof and restore snapshot
echo "powering off and restore"
/c/Program\ Files/Oracle/VirtualBox/VBoxManage.exe controlvm "Win7de64_ACAD2015" poweroff
/c/Program\ Files/Oracle/VirtualBox/VBoxManage.exe snapshot  "Win7de64_ACAD2015" restorecurrent

