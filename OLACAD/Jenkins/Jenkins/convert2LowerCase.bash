#!/bin/bash


##############################################
#
# conversion script to moify filenames to lowecase
#
##############################################

# zip all directories given as command line option
# deflate and implicitely convert to lowercase
zip -0 -r -q tmpFile.zip "$@" && unzip -LL -q tmpFile.zip
rm tmpFile.zip
