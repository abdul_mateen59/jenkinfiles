from __future__ import print_function
import sys
import virtualbox
import time

import logger
###############################################################
returnSaved     =  0     #success
returnBusy      = -1
returnNotFound  = -2
returnOff       = -3
returnUnknown   = -5
#    returnState     =  -255

def getFreeVm(vmConfig):
    """get the VM-name of a free VM out of a list of VMs with vmConfig

    Args:
        vmConfig (str): String naming the requested virtual machine configuration 
                        e.g. Win7de64_ACAD2018
    Returns:
        vmName (str)      if returnState = returnSaved: complete name of the available virtual machine (e.g. Win7de64_ACAD2016-1)
                          else complete name of the last virtual machine name checked
        returnState (int) returnSaved if free saved VM found
                          returnNotFound otherwise
    """

    vbox        = virtualbox.VirtualBox()
    session     = virtualbox.Session()
    machineList = ["", "-0", "-1"]
    fullVmName  = vmConfig

    for machineNr in machineList:
        fullVmName = vmConfig + machineNr
        logger.logInfo( "check machine: " + fullVmName )
        try:
            testMachine = vbox.find_machine( fullVmName )
            vmState     = testMachine.state

            if vmState == virtualbox.library.MachineState.powered_off:
                logger.logInfo("...the machine is ... ", "powered_off")
                returnState = returnOff

            elif vmState == virtualbox.library.MachineState.running:
                logger.logInfo("...the machine is ... ", "running")
                returnState = returnBusy

            elif vmState == virtualbox.library.MachineState.saved:
                # found vm in required state
                logger.logInfo("...the machine is ... ", "saved")
                returnState = returnSaved
                break

            else:
                logger.logInfo("...the machine is ... ", str(vmState) )
                returnState = returnUnknown
        except:
            print(sys.exc_info()[0])
            returnState = returnNotFound # set return state but continue search for vm 

    if returnState != returnSaved:
        returnState = returnNotFound
    return returnState, fullVmName


    ######function to start the virtual machine####
def startVm(fullVmName, vmFrontEnd):
    """Starts a new virtual machine.with the fully qualified name

    Args:
        fullVmName (str): name of the machine to start
        vmFrontEnd (str): 'headless' or 'gui'

    Returns:
        session (ISession) that has a lock on the vm
        testMachine(IMachine) that was started
        vmStartState (ISnapshot) the last snapshot of the vm
    """

    logger.logWithFrame(["Starting virtual machine " , fullVmName])

    vbox         = virtualbox.VirtualBox()
    session      = virtualbox.Session()
    testMachine  = vbox.find_machine(fullVmName)
    vmStartState = testMachine.current_snapshot
    vmVersion    = vbox.version

    progress     = testMachine.launch_vm_process(session, vmFrontEnd, '')  # this will lock the machine for this session
    progress.wait_for_completion()

    logger.logInfo('Successfully started virtual machine', fullVmName)

    return session, testMachine, vmStartState, vmVersion


def restoreVmSnapshot(session, machine, snapshot):
    """Restores a snapshot on a running virtual machine.

    Args:
        session (ISession): session that has locked the machine
        machine (IMachine): machine to power down and restore
        snapshot (ISnapshot): snapshot to restore
    """

    progress = session.console.power_down() # this will unlock the machine
    progress.wait_for_completion(30000)

    logger.logInfo(' - waiting for state poweredOff')
    # the vm can already report poweredOff and is still not finished waiting for some ui or other stuff
    # we need to wait a bit longer
    time.sleep(15)
    waitCount = 0
    while machine.state != virtualbox.library.MachineState.powered_off and (waitCount < 10):
        logger.logInfo(' - waiting for state poweredOff')
        time.sleep(10)
        waitCount += 1

    logger.logInfo(' - lock for restoring snapshot')
    machine.lock_machine(session, virtualbox.library.LockType.write) # get a new lock for snapshot restoration

    logger.logInfo(' - restore snapshot')
    progress = session.machine.restore_snapshot(snapshot)
    progress.wait_for_completion(30000)

    session.unlock_machine()


def run(vmName):
    """check, if a vm is available and prints the result of the check on console
    
    Args:
        vmName (str): name of the machine to check
    """
    try:
        ret, fullVmName = getFreeVm(vmName)
        logger.logInfo(["getFreeVm returned:", ret, fullVmName])
    except:
        print(sys.exc_info()[0])

############################################
if __name__ == '__main__':
    run(sys.argv[1])

