::encrypt GRAPH

set assemblyName=GRAPH.exe
set sourceDir=%WORKSPACE%\Graph\Sources\bin\AnyCPU\NightlyRelease
set sourceFile=%sourceDir%\%assemblyName%
set targetDir=%sourceDir%\Encryption
set targetFile=%targetDir%\%assemblyName%
set axPath=C:\Program Files (x86)\WIBU-SYSTEMS\AxProtector\Devkit\bin

set xmlFile=%targetDir%\AxProtectorSettings.xml

set day=%date:~0,2%
set month_=%date:~3,2%
set year=%date:~-4%

if [%month_%] == [01] set month=Jan
if [%month_%] == [02] set month=Feb
if [%month_%] == [03] set month=Mar
if [%month_%] == [04] set month=Apr
if [%month_%] == [05] set month=May
if [%month_%] == [06] set month=Jun
if [%month_%] == [07] set month=Jul
if [%month_%] == [08] set month=Aug
if [%month_%] == [09] set month=Sep
if [%month_%] == [10] set month=Oct
if [%month_%] == [11] set month=Nov
if [%month_%] == [12] set month=Dec

if not exist %targetDir% md %targetDir%

:: xml-Datei erstellen
echo ^<?xml version="1.0" encoding="utf-8"?^> > "%xmlFile%"
echo ^<AxProtectorNet xmlns:wibu="http://wibu.com/2008/AxpnetControlFile/1.0"^> >> "%xmlFile%"
echo ^<CommandLine^> >> "%xmlFile%"
echo ^<Command^>-kcm^</Command^> >> "%xmlFile%"
echo ^<Command^>-f100825^</Command^> >> "%xmlFile%"
echo ^<Command^>-p200^</Command^> >> "%xmlFile%"
echo ^<Command^>-cf1^</Command^> >> "%xmlFile%"
echo ^<Command^>-rd:%year%%month%%day%,00:00:00^</Command^> >> %xmlFile%
echo ^<Command^>-d:4.50^</Command^> >> "%xmlFile%"
echo ^<Command^>-fw:1.18^</Command^> >> "%xmlFile%"
echo ^<Command^>-slw^</Command^> >> "%xmlFile%"
echo ^<Command^>-ns^</Command^> >> "%xmlFile%"
echo ^<Command^>-ci^</Command^> >> "%xmlFile%"
echo ^<Command^>-cml100^</Command^> >> "%xmlFile%"
echo ^<Command^>-cat336,72^</Command^> >> "%xmlFile%"
echo ^<Command^>-caz^</Command^> >> "%xmlFile%"
echo ^<Command^>-cac15,15^</Command^> >> "%xmlFile%"
echo ^<Command^>-wu10^</Command^> >> "%xmlFile%"
echo ^<Command^>-we5^</Command^> >> "%xmlFile%"
echo ^<Command^>-eac^</Command^> >> "%xmlFile%"
echo ^<Command^>-eec^</Command^> >> "%xmlFile%"
echo ^<Command^>-emc^</Command^> >> "%xmlFile%"
echo ^<Command^>-car30,3^</Command^> >> "%xmlFile%"
echo ^<Command^>-v^</Command^> >> "%xmlFile%"
echo ^<Command^>-cag1^</Command^> >> "%xmlFile%"
echo ^<Command^>-#:"%targetDir%\CodemeterLog.log"^</Command^> >> "%xmlFile%"
echo ^<Command^>-o:"%targetFile%"^</Command^> >> "%xmlFile%"
echo ^<Command^>"%sourceFile%"^</Command^> >> "%xmlFile%"
echo ^</CommandLine^> >> "%xmlFile%"
echo ^<Wupi^> >> "%xmlFile%"
echo ^<!-- intern license --^> >> "%xmlFile%"
echo ^<LicenseList Index="1" Name="1"^> >> "%xmlFile%"
echo ^<License^>1-1^</License^> >> "%xmlFile%"
echo ^</LicenseList^> >> "%xmlFile%"
echo ^<License Name="1-1"^> >> "%xmlFile%"
echo ^<Type^>CodeMeter^</Type^> >> "%xmlFile%"
echo ^<FirmCode^>100825^</FirmCode^> >> "%xmlFile%"
echo ^<ProductCode^>200^</ProductCode^> >> "%xmlFile%"
echo ^<FeatureCode^>2^</FeatureCode^> >> "%xmlFile%"
echo ^<SubSystem^>LocalLanWan^</SubSystem^> >> "%xmlFile%"
echo ^<Access^>NoUserLimit^</Access^> >> "%xmlFile%"
echo ^<MinimumDriverVersion^>4.50^</MinimumDriverVersion^> >> "%xmlFile%"
echo ^<MinimumFirmwareVersion^>1.18^</MinimumFirmwareVersion^> >> "%xmlFile%"
echo ^<UserData^>None^</UserData^> >> "%xmlFile%"
echo ^</License^> >> "%xmlFile%"
echo ^<Namespace Name="Graph" Encryption="true" LicenseList="0"^> >> "%xmlFile%"
echo ^<Class Name="MainForm" Encryption="true" LicenseList="0"^> >> "%xmlFile%"
echo ^<Method Name="CreateChartFormDummy()" Encryption="false"/^> >> "%xmlFile%"
echo ^<Method Name="LoadAppConfig(System.String)" Encryption="false"/^> >> "%xmlFile%"
echo ^<Method Name="SetDockPanels()" Encryption="false"/^> >> "%xmlFile%"
echo ^<Method Name="TestForm_Load(System.Object,System.EventArgs)" Encryption="false"/^> >> "%xmlFile%"
echo ^</Class^> >> "%xmlFile%"
echo ^</Namespace^> >> "%xmlFile%"
echo ^</Wupi^> >> "%xmlFile%"
echo ^</AxProtectorNet^> >> "%xmlFile%"

if exist "%axPath%\AxProtectorNet4.exe" (
 "%axPath%\AxProtectorNet4.exe" @"%xmlFile%"
 echo net4 gefunden
) else (
"%axPath%\AxProtectorNet.exe" @"%xmlFile%"
)
