#!/bin/bash


##############################################
#
# clean up virtual machine state
# 
# powers off given Virtual machine and resets 
# to last snapshot
#
##############################################

poweroff (){

GUEST_VM="$(cat VM_NAME.txt)"
# poweroff and restore to latest snapshot
echo "powering off and restore VM: "
echo " Provided VM $GUEST_VM"
#VBoxManage controlvm "Win7de64_ACAD2015" poweroff

if [ `VBoxManage list runningvms | grep ${GUEST_VM} | wc -l ` -gt 0 ] ; then
    # running VM found
    echo "found GUEST_VM=$GUEST_VM"

	VBoxManage controlvm $GUEST_VM poweroff
	VBoxManage snapshot  $GUEST_VM restorecurrent
else
	echo "GUEST_VM: <${GUEST_VM}> not available or not running"
fi
}

while getopts ':hg:' OPTION ; do
  case "$OPTION" in
    h)   echo "this script powers off and restore to latest snapshot";;
    g)   GUEST_VM=$OPTARG; poweroff;;
    *)   echo "Unknown parameter"
  esac
done
