#!groovy

def String stashBaseName = "OLACAD"
def stashAcadUnitTest = "AcadUnitTest"
def String lastBuildStatus = currentBuild.rawBuild.getPreviousBuild()?.getResult()
cause = "NONE"

///////////////////////////////////////////////////////////////////////////////
// 
// start pipeline
//
///////////////////////////////////////////////////////////////////////////////
node ('master') {

    if (env.gitlabMergeRequestId) {
        updateGitlabCommitStatus name: "$env.JOB_NAME", state: 'pending'
    }
    
    stage ('load Scripts') {
	    rootDir = pwd()
	    scmHandler          = load ("../workspace@script/test/scmHandler.groovy")
	    notificationHandler = load ("../workspace@script/test/notificationHandler.groovy")
	    causeHandler        = load ("../workspace@script/test/buildCauseHandler.groovy")

		def cause 		= causeHandler.getLastBuildCause()
		echo "[INFO] last build Cause:             " + cause
		echo "[INFO] last build Cause description: " + cause.getShortDescription()
	 }
}

node('Win7&&timeSvrAccess&&git') {
  try {
    timestamps {
        stage ('prepare WS') {
            println "this is job: "+JOB_NAME
            println "running on:  "+NODE_NAME
            echo "last status was: ${lastBuildStatus}"
            echo 'clean up workspace...'
            cleanWs(patterns: [[pattern: '*\\bin\\*', type: 'INCLUDE'], 
                               [pattern: '*\\x64\\*', type: 'INCLUDE'], 
                               [pattern: 'OLACAD_Deployment\\OLACAD_Installer\\toDeploy\\*', type: 'INCLUDE'], 
                               [pattern: '*\\obj\\*', type: 'INCLUDE']
                              ])
            echo 'clean up workspace done'
        }

        stage ('SCM') {
            bat "set"
            scmHandler.handleCheckout()
            bat "git branch -vv"
            
            stash name: stashBaseName+"-src",    includes: '**/*.cpp' 
            stash name: stashBaseName+"-header", includes: '**/*.h'
            stash name: stashBaseName+"-asm", includes: '**/Baugruppen/**/*'
        }

        stage('build OLA'){
            dir('\\OLACAD_Deployment\\OLACAD_Build'){
				bat """python main.py --autocad=${env.OBJECTARX_VERSION} --lang=${env.ACAD_LANGUAGE} --feature=${env.FEATURE} --config=Debug
					if ERRORLEVEL 1 goto FAILURE
						echo "[SUCCESS] OLAcad build finished"
						exit /b 0
					:FAILURE
					echo "[FAILURE] OLAcad build completes with error(s)"
					exit /b 1
				"""
			}
        }

        stage('build UTest'){
            dir('AcadUnitTest') {
                bat """\"${tool 'MSBuild2017'}\" AcadUnitTest.vcxproj /p:Configuration=Debug_${env.OBJECTARX_VERSION} /p:Platform=x64
					if ERRORLEVEL 1 goto FAILURE
						echo "[SUCCESS] AutoCad UnitTest build finished"
						exit /b 0
					:FAILURE
					echo "[FAILURE] AutoCad UnitTest build completes with error(s)"
					exit /b 1
				"""
			}
            echo "stash temporary artifacts"
            
            stash name: stashAcadUnitTest, includes: "AcadUnitTest/**/*.arx, AcadUnitTest/**/*.pdb, AcadUnitTest/**/*.dwg, AcadUnitTest/**/*.xml"            
            stash name: stashBaseName+"-msi", includes: "/OLACAD_Deployment/**/*.msi"
            stash name: stashBaseName+"-dll", includes: "OLACAD_Tools/OlacadLoader/bin/Debug/WupiEngineNet.dll"

            echo "stash complete"
        }

        stage('collect artifacts') {
            archiveArtifacts artifacts: "AcadUnitTest/**/*.arx"
            archiveArtifacts artifacts: "**/AcadUnitTest.pdb"
            archiveArtifacts artifacts: "AcadUnitTest/testdwg/*.dwg"
            archiveArtifacts artifacts: "OLACAD_Deployment/**/*.msi"
            archiveArtifacts artifacts: "**/WupiEngineNet.dll"
            archiveArtifacts artifacts: "OLACAD_Deployment/OLACAD_Installer/toDeploy/*.exe"
            archive '.arx, ,.xml, .msi, OLACAD_Deployment/OLACAD_Installer/toDeploy/*.exe'
        }
    } // timestamps
  }
  catch (any) {
    // If there was an exception thrown, the build failed
    echo 'build failed'
    currentBuild.result = "FAILURE"
    notificationHandler.notifyBuild(lastBuildStatus, cause, currentBuild.result)
    echo 'notifyBuild trigered in catch()'
    throw any //rethrow exception to prevent the build from proceeding
  }
} //node

node('LINUX&&VBOX_HOST') {
	timestamps {
		stage('unstash'){
			step([$class: 'WsCleanup'])
			unstash stashBaseName+"-src"
			unstash stashBaseName+"-header"
            unstash stashBaseName+"-asm"
            unstash stashAcadUnitTest
			unstash stashBaseName+"-dll"

			step([$class: 'CopyArtifact', filter: '**/*arx', 
										fingerprintArtifacts: true, flatten: true, 
										projectName: JOB_NAME, 
										selector: [$class: 'SpecificBuildSelector', buildNumber: '${BUILD_NUMBER}'], 
										target: './AutocadUnitTest.bundle'])

			step([$class: 'CopyArtifact', filter: '**/*.dwg', 
										fingerprintArtifacts: true, flatten: true, 
										projectName: JOB_NAME, 
										selector: [$class: 'SpecificBuildSelector', buildNumber: '${BUILD_NUMBER}'], 
										target: './testdwg'])

			step([$class: 'CopyArtifact', filter: '**/*pdb',
										fingerprintArtifacts: true, flatten: true, 
										projectName: JOB_NAME, 
										selector: [$class: 'SpecificBuildSelector', buildNumber: '${BUILD_NUMBER}'], 
										target: './'])
										
			step([$class: 'CopyArtifact', filter: '**/Debug/WupiEngineNet.dll',
										fingerprintArtifacts: true, flatten: true, 
										projectName: JOB_NAME, 
										selector: [$class: 'SpecificBuildSelector', buildNumber: '${BUILD_NUMBER}'], 
										target: './'])
										
			step([$class: 'CopyArtifact', filter: '**/*msi', 
										fingerprintArtifacts: true, flatten: true, 
										projectName: JOB_NAME, 
										selector: [$class: 'SpecificBuildSelector', buildNumber: '${BUILD_NUMBER}'], 
										target: './'])
		}
	
		stage ('SCM UTest') {
			// scm checkout jenkins scripts for handling unittest
			sh 'echo ================================'
			sh 'echo $(pwd)'
			sh 'echo ================================'
			checkout changelog: false, poll: false, scm: [$class: 'GitSCM', 
                                                            branches: [[name: '*/devfix/OLD-200_repair_syscleanup']], 
                                                            doGenerateSubmoduleConfigurations: false, 
                                                            extensions: [[$class: 'RelativeTargetDirectory', 
                                                            relativeTargetDir: 'scripts'], 
                                                            [$class: 'CloneOption', depth: 0, noTags: false, reference: '', shallow: true]], 
                                                            submoduleCfg: [], 
                                                            userRemoteConfigs: [[credentialsId: 'jenkins_2019-01-29', url: 'git@gitlab:OLAcad/jenkins.git']]]
		}
    
		stage('UnitTest'){
			try{
				parallel a:{
					echo "Enter unittest A"
					echo "ACAD${env.OBJECTARX_VERSION}_VM resource"
					if ( "$NODE_NAME" == "VMHost_Lin-0" ) {
						LOCKABLE_RESOURCE="ACAD${env.OBJECTARX_VERSION}_VM"
					} else {
						LOCKABLE_RESOURCE="ACAD${env.OBJECTARX_VERSION}_VM1"
					}
					
					println "host:     "+NODE_NAME
					println "running:  "+LOCKABLE_RESOURCE

					lock ("$LOCKABLE_RESOURCE"){
						echo "================================"
						def myDir = env.WORKSPACE
						echo 'WORKSPACE: '+myDir.toString()
						echo "================================"
						sh 'ls -l'

						dir(myDir.toString()+'/scripts'){
							sh "python runAcadUnitTest.py ${env.OS_VERSION} ${env.ACAD_LANGUAGE} ${env.OBJECTARX_VERSION} headless \"D:\\jenkins\\workspace\\${env.JOB_NAME}\" D:/olacad_test/ D:/testDwg/ ${env.FEATURE}" 
						}
					} //lock
					echo "unlock $LOCKABLE_RESOURCE"
					echo 'set temp build status to SUCCESS'
					currentBuild.result = 'SUCCESS'
				},
				b:{
					echo "Enter unittest B for 2020"
					echo "ACAD${env.OBJECTARX_VERSION}_VM resource"
					if( env.OBJECTARX_VERSION == "2019"){
							if ( "$NODE_NAME" == "VMHost_Lin-0" ) {
								LOCKABLE_RESOURCE="ACAD2020_VM"
							} else {
								LOCKABLE_RESOURCE="ACAD2020_VM1"
							}

							println "host:     "+NODE_NAME
							println "running:  "+LOCKABLE_RESOURCE

							lock ("$LOCKABLE_RESOURCE"){
								echo "================================"
								def myDir = env.WORKSPACE
								echo 'WORKSPACE: '+myDir.toString()
								echo "================================"
								sh 'ls -l'

								dir(myDir.toString()+'/scripts'){
									sh "python runAcadUnitTest.py ${env.OS_VERSION} ${env.ACAD_LANGUAGE} 2020 headless \"D:\\jenkins\\workspace\\${env.JOB_NAME}\" D:/olacad_test/ D:/testDwg/ ${env.FEATURE}" 
								}
							} //lock	
							echo "unlock $LOCKABLE_RESOURCE"
							echo 'set temp build status to SUCCESS'
							currentBuild.result = 'SUCCESS'					
						}		
					}
				}   
			catch (any) {
				// If there was an exception thrown, the build failed
				echo 'build failed'
				currentBuild.result = "FAILURE"
                // needs NODE_NAME
				build job: 'SYS_cleanUpVM', 
                    parameters: 
                    [
                        [ $class: 'StringParameterValue', name: 'VM_NAME', value: "${env.OS_VERSION}64_ACAD${env.OBJECTARX_VERSION}"], 
                        [ $class: 'NodeParameterValue', name:    'UPSTREAM_NODE', 
                                                        labels: ["$NODE_NAME"], 
                                                        nodeEligibility: [$class: 'IgnoreOfflineNodeEligibility'] 
                        ]
                    ],
                    wait: false

                if (env.gitlabMergeRequestId) {
                    updateGitlabCommitStatus name: "$env.JOB_NAME", state: 'failed'
                }                
				throw any //rethrow exception to prevent the build from proceeding
			}
			finally {
				if (currentBuild.result != "FAILURE"){
					junit 'testResult_*/olacad_test/junit_result.xml'
					archiveArtifacts artifacts: "testResult_*/olacad_test/junit_result.xml"
					sh 'ls -l testResult_*/olacad_test/junit_result.xml'

					sh 'ls -l testResult_*/AcadUnitTestCoverage.*'
					archiveArtifacts artifacts: "testResult_*/AcadUnitTestCoverage.*"
					//not yet available??
					archiveArtifacts artifacts: "testResult_*/CoverageReport/*"
					echo testStatuses()
				}
				echo 'finally block done'
				// send notifications on Success
	//            step([$class: 'Mailer', notifyEveryUnstableBuild: true, recipients: 'me@me.com', sendToIndividuals: true])
			}        
		} // stage
		stage('MsiTest'){
			try{
				echo "enter msi_test"
					echo "ACAD${env.OBJECTARX_VERSION}_VM resource"
					if ( "$NODE_NAME" == "VMHost_Lin-0" ) {
						LOCKABLE_RESOURCE="ACAD${env.OBJECTARX_VERSION}_VM"
					} else {
						LOCKABLE_RESOURCE="ACAD${env.OBJECTARX_VERSION}_VM1"
					}

					lock ("$LOCKABLE_RESOURCE"){
						echo "================================"
						def myDir = env.WORKSPACE
						echo 'WORKSPACE: '+myDir.toString()
						echo "================================"
						sh 'ls -l'

						dir(myDir.toString()+'/scripts'){
							sh "python runMsiTest.py ${env.OS_VERSION} ${env.ACAD_LANGUAGE} ${env.OBJECTARX_VERSION} headless ${env.FEATURE}" 
						}
					} //lock					
				echo "unlock $LOCKABLE_RESOURCE"
				echo 'set temp build status to SUCCESS'
				currentBuild.result = 'SUCCESS'						
			}
			catch (any) {
				// If there was an exception thrown, the build failed
				echo 'build failed'
				currentBuild.result = "FAILURE"
                // needs NODE_NAME
				build job: 'SYS_cleanUpVM', 
                    parameters: 
                    [
                        [ $class: 'StringParameterValue', name: 'VM_NAME', value: "${env.OS_VERSION}64_ACAD${env.OBJECTARX_VERSION}"], 
                        [ $class: 'NodeParameterValue', name:    'UPSTREAM_NODE', 
                                                        labels: ["$NODE_NAME"], 
                                                        nodeEligibility: [$class: 'IgnoreOfflineNodeEligibility'] 
                        ]
                    ],
                    wait: false

                if (env.gitlabMergeRequestId) {
                    updateGitlabCommitStatus name: "$env.JOB_NAME", state: 'failed'
                }                
				throw any //rethrow exception to prevent the build from proceeding
			}
			finally {
				if (currentBuild.result != "FAILURE"){
					// junit file generation not yet supported
//					junit 'olacad_test/junit_result.xml'
//					archiveArtifacts artifacts: "olacad_test/junit_result.xml"
//					sh 'ls -l olacad_test/junit_result.xml'
					echo testStatuses()
				}
				echo 'finally block done'
				// send notifications on Success
	//            step([$class: 'Mailer', notifyEveryUnstableBuild: true, recipients: 'me@me.com', sendToIndividuals: true])
			}        
		} // stage
///////////////////////////////////
		echo 'notifyBuild triggered'
		notificationHandler.notifyBuild(lastBuildStatus, cause, currentBuild.result)
		echo "RESULT: ${currentBuild.result}"
	} // timestamps
}//node 

//=========================================
// helper function declarations
//=========================================
import hudson.tasks.test.AbstractTestResultAction

// for help on @NonCPS look here:
// https://github.com/jenkinsci/workflow-cps-plugin/blob/master/README.md#technical-design
@NonCPS
def testStatuses() {
    def testStatus = ""
    AbstractTestResultAction testResultAction = currentBuild.rawBuild.getAction(AbstractTestResultAction.class)
//    if (testResultAction != null) {
    if(currentBuild.result != null && !"SUCCESS".equals(currentBuild.result)) {
        def total = testResultAction.totalCount
        def failed = testResultAction.failCount
        def skipped = testResultAction.skipCount
        def passed = total - failed - skipped
        testStatus = "Test Status:\n  Passed: ${passed}, Failed: ${failed} ${testResultAction.failureDiffString}, Skipped: ${skipped}"
        
        currentBuild.result = 'UNSTABLE'
        if (failed == 0) {
            currentBuild.result = 'SUCCESS'
        }
    }
    else {
		echo "currentBuild.result empty or != SUCCESS"
    }
    return testStatus
}

import hudson.model.Actionable

@NonCPS
def getTestSummary = { ->
    def testResultAction = currentBuild.rawBuild.getAction(AbstractTestResultAction.class)
    def summary = ""

    if (testResultAction != null) {
        def total = testResultAction.getTotalCount()
        def failed = testResultAction.getFailCount()
        def skipped = testResultAction.getSkipCount()

        summary = "Test results:\n\t"
        summary = summary + ("Passed: " + (total - failed - skipped))
        summary = summary + (", Failed: " + failed)
        summary = summary + (", Skipped: " + skipped)
    } else {
        summary = "No tests found"
    }
    return summary
}
