#!/bin/bash

#set -e

if [[ $MD_DEBUG_ENABLED -ge 1 ]];
then 
	echo "[INFO] running in debug mode"
fi

EXIT_STATUS=0
KMI_ROOT_DIR=/opt/tomcat/webapps/ROOT/

export GRAILS_OPTS="-Xverify:none -Xmx4096m -Xms4096m -XX:PermSize=2048m -XX:MaxPermSize=4096m -Ddisable.auto.recompile=true -XX:SurvivorRatio=128 -XX:MaxTenuringThreshold=0 -XX:+UseTLAB -XX:+UseConcMarkSweepGC -XX:+CMSClassUnloadingEnabled -XX:+CMSIncrementalMode -XX:-UseGCOverheadLimit -XX:+ExplicitGCInvokesConcurrent"
       
echo "Überprüfe zunächst, ob Grails bereits im PATH gesetzt ist."
if echo $PATH | grep -q "/opt/grails"
then
    echo "Grails war bereits gesetzt!"
else
    echo "Grails war nicht gesetzt, also muss es gesetzt werden!"
    export GRAILS_HOME=/opt/grails
    export PATH=$PATH:$GRAILS_HOME/bin
fi

#echo "Jetzt wird zunächst das Workspace Directory aufgeräumt!"
grails -Dgrails.work.dir=$WORKSPACE clean || EXIT_STATUS=$?

echo "Jetzt werden alle Klassen compiliert!"
grails -Dgrails.work.dir=$WORKSPACE compile || EXIT_STATUS=$?

BUILD_RUN=0
TEST_RUN=0

while getopts 'bht' OPTION ; do
  case "$OPTION" in
    b)   BUILD_RUN=1;;
    h)   echo "enter -b for build or -t for test"; exit 1;;
    t)   TEST_RUN=1;;
    *)   echo "Unknown parameter, use -h for help"; exit 1;;
  esac
done

if [[ $TEST_RUN == 1 ]];
then
    PACKAGES_TO_TEST="*UrlMappings api.A* api.F* api.G* api.K* api.M* auswertung dashboard ebe fahrt filter filter.kmi filter.kundengarantie filter.wissensmanagement finanzen fundsache helpdesk kmi.A* kmi.B* kmi.D* kmi.E* kmi.F* kmi.G* kmi.H* kmi.I* kmi.J* kmi.K* kmi.L* kmi.M* kmi.N* kmi.P* kmi.R* kmi.S* kmi.T* kmi.UpdateController kmi.UrlUtil kmi.V* kmi.W* kmi.Z* kmi.datenLoeschen kontaktformular kundengarantie kundengarantie.fahrplanauskunft vertrieb wissensmanagement wissensmanagement.versand wopi"
    echo "############################################"
    echo "[INFO] these packages will be tested:"
    for package in ${PACKAGES_TO_TEST}
    do
      echo "- $package"
    done
    
    echo "############################################"
    #DEBUG ON
    set -x
    
    ls -l $WORKSPACE/target/test-reports/
    for paket in ${PACKAGES_TO_TEST}
    do
        echo "[INFO] Nun beginnen die Tests für Paket '${paket}'!"
        if echo x"$paket" | grep '*' > /dev/null;
        then
          grails -Dgrails.work.dir=$WORKSPACE test-app -unit ${paket} || EXIT_STATUS=$?
        else 
          grails -Dgrails.work.dir=$WORKSPACE test-app -unit ${paket}.* || EXIT_STATUS=$?
        fi
        echo "[INFO] Sichere die Tests dieses Durchlaufs!"
        ls -l $WORKSPACE/target/test-reports/
        PACKAGENAME_WITHOUT_DOTS="${paket/\./_}"
        PACKAGENAME_WITHOUT_STARS="${paket/\*/_}"
        if [ -e "$WORKSPACE/target/test-reports/TESTS-TestSuites.xml" ]
        then
            mv "$WORKSPACE/target/test-reports/TESTS-TestSuites.xml" "$WORKSPACE/TESTS-TestSuites_${PACKAGENAME_WITHOUT_STARS}.xml"
        else
            mv "$WORKSPACE/target/test-reports/TEST-unit-unit-${paket}" "$WORKSPACE/"
        fi
        echo "[INFO] Tests gesichert! Exit Code lautet bis hierhin: $EXIT_STATUS"
        sleep 2
        echo "[INFO] 2 Sekunden Ruhe gelassen fuer die Kiste!"
    done
    
    #echo "Teste jetzt noch das Javascript!"
    if npm list -g | grep "jshint"
    then
        echo "JSHint ist installiert"
    else
        echo "JSHint ist NICHT installiert, daher installiere es!"
        npm install -g jshint
        echo "JSHint ist nun installiert"
    fi
    
    jshint --reporter=testverzeichnis/reporter.js grails-app/assets/javascripts/kmi/. > "TESTS-TestSuites_javascript.xml"
    
    if [ -f "$WORKSPACE/testverzeichnis/Frontendtests.xml" ];
    then
        mv "$WORKSPACE/testverzeichnis/Frontendtests.xml" "$WORKSPACE/TESTS-TestSuites_Frontend.xml"
    fi
fi

if [[ $BUILD_RUN == 1 ]];
then
    echo "[INFO] Starte die Erstellung einer Archiv Datei (war)."
    grails -Dgrails.work.dir=$WORKSPACE war || EXIT_STATUS=$?
    
    if [[ $EXIT_STATUS == 0 ]];
    then
        KUNDENVERSIONEN="augsburg bls braunschweig db gvh hochbahn hvv lvs metronom nahbus sbahnhh sl ssb ssbtest svt vbn vhh vsn vvs vvt vorarlberg"
        for kunde in ${KUNDENVERSIONEN}
        do
            echo "[INFO] Baue nun KMI fuer Kunde '${kunde}' (Umgebung production_${kunde})!"
            grails -Dgrails.work.dir=$WORKSPACE -Dgrails.env=production_${kunde} war || EXIT_STATUS=$?
        done
    fi
fi

echo "[INFO] Alles ist fertig :-) Exit Code lautet bis hierhin: $EXIT_STATUS"

#DEBUG OFF
set +x
exit $EXIT_STATUS
