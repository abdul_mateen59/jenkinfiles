import __main__
import os
import sys
import shutil
import virtualbox
import subprocess as sp
import time
#import zipfile
import tarfile
import logger
import vmHandler

version = 'running on python version {0.major}.{0.minor}.{0.micro}'.format(sys.version_info)
logger.logInfo(version)

vmVersion = u'5.2.0'
acadVersion =  '2020'

if (sys.version_info < (3, 4)):
  logger.logInfo( "using pathlib2")
  import pathlib2 as pathlib
else:
  logger.logInfo( "using pathlib")
  import pathlib


from pathConfig import WorkspacePath, VmPath
#################################################################
#
#
# PLEASE add documentation, what's going on here
#
#################################################################

# credentials for vm guest session
username = 'admin'
password = 'ac4tion!'

def guaranteeDirectory(guestSession, directory):
    """Creates (recursively) the full path specified in directory if it does not exists.
    Example: if directory = "D:\\jenkins\\workspace\\OLACAD" and "D:\\jenkins" already exists 
    on the guest, "D:\\jenkins\\workspace" and "D:\\jenkins\\workspace\\OLACAD" will be
    created.
    functions used see https://pythonhosted.org/pyvbox/virtualbox/library.html?highlight=makedirs#virtualbox.library.IGuestSession
    (low level ?) 
    like directory_exists instead of directoryExists
    """
    def isDriveLetter(directory):
        return len(directory) == 2 and directory[-1] == ':'
    
    if directory == '' or isDriveLetter(directory):
        return
    try:
        # if dir is not existing exception can be thrown !
        if not guestSession.directory_exists(directory):
            logger.logInfo('Creating', directory)
            guestSession.makedirs(directory)
            if guestSession.directory_exists(directory):
                return
            #guaranteeDirectory(guestSession, '\\'.join(directory.split('\\')[:-1])) # guarantee parent directory exists
            #guestSession.directory_create(directory, 1, [])
    except:
        logger.logInfo('Creating', directory)
        try:
            guestSession.makedirs(directory)
            if guestSession.directory_exists(directory):
                return
        except:
            guaranteeDirectory(guestSession, '\\'.join(directory.split('\\')[:-1])) # guarantee parent directory exists
            #guestSession.directory_create(directory, 1, [])


def copy_tree(guestSession, hostPath, guestPath, vmVersion):
    """Copies the directory tree starting at hostPath from the host machine
    to the directory guestPath on the guest machine.

    Args:
        gs (IGuestSession): guestSession used to copy files
        hostPath (WorkspacePath): path configuration on host machine
        guestPath (VmPath): path configuration on guest machine
    """

    for src_dir, dirs, files in os.walk(hostPath):
        dst_dir = src_dir.replace(hostPath, guestPath, 1)
        winTargetPath = pathlib.PureWindowsPath(dst_dir)
        guaranteeDirectory(guestSession, str(winTargetPath))
        for file_ in files:
            src_file = os.path.join(src_dir, file_)
            winTargetFile = winTargetPath / file_
            if vmVersion == u'5.2.8':
                p = guestSession.file_copy_to_guest(src_file, str(winTargetPath), [])
            else:
                p = guestSession.file_copy_to_guest(src_file, str(winTargetFile), [])
            p.wait_for_completion()


def deployFiles(gs, hostPath, guestPath, vmVersion):
    """Copies all files for test execution from host to guest.

    Args:
        gs (IGuestSession): guestSession used to copy files
        hostPath (WorkspacePath): path configuration on host machine
        guestPath (VmPath): path configuration on guest machine
    """
    
    logger.logWithFrame(['Copying files from host to virtual machine'])

    ### source files
    logger.logInfo('Copying olacad source files')
    copy_tree(gs, hostPath.olacadSourcePath, guestPath.olacadRootDir, vmVersion)

    ### data files
    logger.logInfo('Copying test dwg files')
    copy_tree(gs, hostPath.testDrawingsSourcePath, guestPath.testDrawingsPath, vmVersion)

    ### unit test files
    logger.logInfo('Copying AcadUnitTest bundle')
    guaranteeDirectory(gs, guestPath.unitTestBundlePath)
    winTargetPath = pathlib.PureWindowsPath(guestPath.unitTestBundlePath)
    winTargetFile = winTargetPath / 'PackageContents.xml'
    if vmVersion == u'5.2.8':
        p = gs.file_copy_to_guest(hostPath.unitTestBundlePackage, str(winTargetPath), [])
    else:
        p = gs.file_copy_to_guest(hostPath.unitTestBundlePackage, str(winTargetFile), [])
    logger.logInfo(p.description)
    p.wait_for_completion()

    guaranteeDirectory(gs, guestPath.unitTestBinPath)
    winTargetPath = pathlib.PureWindowsPath(guestPath.unitTestBinPath)
    for uTestFile in hostPath.unitTestFiles:
        pu = pathlib.PurePath(uTestFile)
        winTargetFile = winTargetPath / pu.name
        if vmVersion == u'5.2.8':
            p = gs.file_copy_to_guest(uTestFile, str(winTargetPath), [])
        else:
            p = gs.file_copy_to_guest(uTestFile, str(winTargetFile), [])
        logger.logInfo(p.description)
        p.wait_for_completion()

    ### junit target dir 
    logger.logInfo('Create AcadUnitTest test result path')
    guaranteeDirectory(gs, guestPath.testResultPath)

    ### scripts for local execution of unit test on vm
    logger.logInfo('Copying unit test execution scripts')
    winTargetPath = pathlib.PureWindowsPath(guestPath.scriptPath)
    for scriptFile in hostPath.scriptFiles:
        sourceFile = os.path.join(hostPath.scriptSourcePath, scriptFile)
        winTargetFile = winTargetPath / scriptFile
        if vmVersion == u'5.2.8':
            p = gs.file_copy_to_guest(sourceFile, str(winTargetPath), [])
        else:
            p = gs.file_copy_to_guest(sourceFile, str(winTargetFile), [])
        logger.logInfo(p.description)
        p.wait_for_completion()
        if p.result_code != 0:
            logger.logError('Error when copying', sourceFile)
            logger.logError(p.error_info.text)


def startUnitTests(gs, guestPath, acadLanguage, acadVersion, feature):
    """Triggers unit test execution on the guest machine.
    Therefore the test execution script is executed on the guest via vbox api.
    All files necessary for test execution (including the test execution script)
    have to be present on the guest for this function to work.

    Args:
        gs (IGuestSession): guestSession used to run unit test script on vm        
        guestPath (VmPath): path configuration on guest machine
        acadLanguage (str): AutoCad language, e.g. 'de-DE'
        acadVersion (str): AutoCad version, e.g. '2016'
        feature (str): olacad feature 'Normal', 'Lite'
    """

    logger.logWithFrame(['Starting unit test execution script on virtual machine'])

    environVars = {}
    environVars['JUNIT_OUT_DIR'] = guestPath.testResultPath
    environVars['TESTDWG_DIR'  ] = guestPath.testDrawingsPath
    
    envVarList = [var + '=' + environVars[var] for var in environVars]
    
    logger.logInfo('setting environment variables on guest')
    for var in envVarList:
        logger.logInfo(var)

    ### Using IGuestSession.execute for test execution we have to wait for the
    ### test execution script to finish in order to get stdout and stderr.
    ### Alternatively we could also start a process on the VM asynchronously.
    ### We could then probably flush stdout and stderr during the test execution.

    process, stdout, stderr = gs.execute(guestPath.pythonExe, [guestPath.testExecScriptPath, acadLanguage, acadVersion, guestPath.buildWorkspacePath, feature], '', envVarList)

    infoFromVm  = ['VM Log ' + line for line in stdout.split('\n') if 'skipped' not in line]
    errorFromVm = ['VM Log ' + line for line in stderr.split('\n') if 'skipped' not in line]

    for line in infoFromVm:
        logger.logInfo(line)
    for line in errorFromVm:
        logger.logError(line)

    logger.logInfo('Unit test execution has finished, waiting for test results')

    #if acadVersion == '2019':
        #logger.logInfo('start Notepad++ for Debuging purpose')
        #process, stdout, stderr = gs.execute('d:\toolbox\NPPlus\notepad++.exe')
        #process, stdout, stderr = gs.execute(r'd:\toolbox\NPPlus\notepad++.exe', [], '', envVarList)
        #logger.logWithFrame(['python script notepad++ done'])


def gatherTestResults(gs, hostPath, guestPath):
    """Copies the zipped test results from guest to host
    and extracts the zip to the workspace.

    Args:
        gs (IGuestSession): guestSession used to copy files
        hostPath (WorkspacePath): path configuration on host machine
        guestPath (VmPath): path configuration on guest machine
    """

    logger.logWithFrame(['Gathering test results', 'copying files from virtual machine to host'])

    #p = gs.file_copy_from_guest(guestPath.testResultZip, hostPath.testResultZip, [])
    p = gs.file_copy_from_guest(guestPath.testResultTar, hostPath.testResultTar, [])
    logger.logInfo(p)
    p.wait_for_completion(2000)

    try:
        #testResults = zipfile.ZipFile(hostPath.testResultZip, 'r')
        testResults = tarfile.TarFile(hostPath.testResultTar, 'r')
        ExtDir=hostPath.testResultTar
        ExtDir = ExtDir[:-4]
        testResults.extractall(ExtDir)
        #testResults.extractall(hostPath.workspace)
        testResults.close()
    #except zipfile.BadZipfile:
    except tarfile.TarError:
        logger.logInfo("Error when extracting test results, but the results seem to be available nonetheless.")
        logger.logInfo("Ignoring this error for now")
    except:
        raise

def resolveCommandLineOptions(argv):
    """primitive method to map the command line arguments to variables
    
    Args:
        argv (argv): options from command line

    Returns:
        acadLanguage
        acadVersion
        vmName
        vmFrontend
        buildWorkspacePath
        junitOutDir
        testDwgDir
        feature
    """

    # default parameters, used when number of command line options does not match the expected number (8)
    OSVersion    = 'Win7de'
    acadLanguage = 'de-DE'
    acadVersion  = '2019'
    vmFrontend   = 'gui'
    buildWorkspacePath = 'OLACADbuild'
    junitOutDir  = 'D:\\olacad_test\\'
    testDwgDir   = 'D:\\testdwg\\'
    feature      = 'Normal'

    expectedArgCount = 8
    if len(argv) == expectedArgCount:
        logger.logInfo('Using command ' + str(expectedArgCount) + ' line arguments:')
        OSVersion    = argv[0]
        acadLanguage = argv[1]
        acadVersion  = argv[2]
        vmFrontend   = argv[3]
        buildWorkspacePath = argv[4]
        junitOutDir  = argv[5]
        testDwgDir   = argv[6]
        feature      = argv[7]
    elif len(argv) == (expectedArgCount-1): # assuming legacy call without OS type
        logger.logInfo('Using command ' + str((expectedArgCount-1)) + ' line arguments:')
        OSVersion    = 'Win7de'
        acadLanguage = argv[0]
        acadVersion  = argv[1]
        vmFrontend   = argv[2]
        buildWorkspacePath = argv[3]
        junitOutDir  = argv[4]
        testDwgDir   = argv[5]
        feature      = argv[6]
    else:
        logger.logWarn('Expected number of arguments: ' + str(expectedArgCount))
        logger.logWarn('Supplied number of arguments: ' + str(len(argv)))
        logger.logWarn('Using default parameters:')
    
    vmName = OSVersion + '64_ACAD' + acadVersion

    logger.logInfo('OS-Version:     ', OSVersion)
    logger.logInfo('Acad language:  ', acadLanguage)
    logger.logInfo('Virtual Machine:', vmName)
    logger.logInfo('Front end:      ', vmFrontend)
    logger.logInfo('JUNIT_OUT_DIR:  ', junitOutDir)
    logger.logInfo('TESTDWG_DIR:    ', testDwgDir)
    logger.logInfo('FEATURE:        ', feature)
    
    return acadLanguage, acadVersion, vmName, vmFrontend, buildWorkspacePath, junitOutDir, testDwgDir, feature

def run(argv):
    """runs AutoCad-Unittests on a virtual machine.
    Arguments have to be supplied in fixed order and number

    Args (all str):
        acadLanguage: e.g. 'de-DE'
        acadVersion: AutoCad version to use for running unit tests, e.g. '2016'
        vmFrontend: 'gui' or 'headless'
        buildWorkspacePath: the absolute path that was used to build olacad and the unit tests (must be the same as in AcadUnitTest.pdb, otherwise coverage won't find the olacad source files) - problem with build on drive C: and running on VM drive D:
        junitOutDir: path on the virtual that will be used by the unit tests to write their result file
        testDwgDir: path on the virtual machine where unit tests can find test dwg files
        feature: program feature 'Lite' or 'Normal'
    """

    logger.logWithFrame(['Running AcadUnitTest'])
    acadLanguage, acadVersion, vmName, vmFrontend, buildWorkspacePath, junitOutDir, testDwgDir, feature = resolveCommandLineOptions(argv)

    hostPath = WorkspacePath(acadVersion, feature, acadLanguage)
    guestPath = VmPath(acadVersion, buildWorkspacePath, feature, acadLanguage)
        
    logger.logInfo('')
    logger.logInfo('Current jenkins workspace on host machine is', hostPath.workspace)

    vmstate, vmName = vmHandler.getFreeVm(vmName)
    try:
        f=open("VM_NAME.txt",'w+')
        f.write(vmName)
        logger.logInfo('VM_NAME File Created ' + str(vmName))
    except IOError:
        logger.logInfo("Unable to create file on disk.")
    finally:
        f.close()

    if vmstate == vmHandler.returnSaved:
        session, testMachine, vmStartState, vmVersion = vmHandler.startVm(vmName, vmFrontend)
    else:
        raise Exception("VM not available")
        
    logger.logInfo('Logging into virtual machine as user', username)
    guestSession = session.console.guest.create_session(username, password)  

    try:
        deployFiles(guestSession, hostPath, guestPath, vmVersion)
        startUnitTests(guestSession, guestPath, acadLanguage, acadVersion, feature)
        time.sleep(4) # assure proper result file state in vm

        gatherTestResults(guestSession, hostPath, guestPath)
        time.sleep(4)
    except:
        logger.logError(sys.exc_info()[0])
    finally:
        logger.logWithFrame(['Logging out user', username])
        guestSession.close()

    logger.logWithFrame(['shutting down virtual machine and restoring snapshot'])
    vmHandler.restoreVmSnapshot(session, testMachine, vmStartState)

    return session, guestSession, testMachine, vmStartState

if __name__ == '__main__':
    session, guestSession, vm, vmStartState = run(sys.argv[1:])

