#!groovy

//////////////////////////////////////////////////////////////////
//
// library for notification handling functions
// info how to use the ext-email-plugin ni pipeline can be found here:
// https://jenkins.io/doc/pipeline/steps/email-ext/
//
//////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////
//
// function to handle build notifications
//
//////////////////////////////////////////////////////////////////
import hudson.tasks.test.AbstractTestResultAction
import hudson.model.Actionable

def notifyBuild(String lastBuildStatus, String buildCause, String buildStatus = 'NOT_BUILT') {
  // build status of null means successful
  buildStatus =  buildStatus ?: 'SUCCESS'
  buildChangeStatus = ''
  echo 'notifyBuild_Start'
  echo "buildStatus     = ${buildStatus}"
  echo "LastBuildStatus = ${lastBuildStatus}"
  echo "buildCause      = ${buildCause}"
  
  if (!lastBuildStatus.equals(currentBuild.result)){
    // if result has changed
    if(lastBuildStatus.equals("SUCCESS") && !currentBuild.result.equals('SUCCESS')){
        // changed from SUCCESS to FAILURE or UNSTABLE OR Aborted
        buildChangeStatus = 'is broken'
    } else if(!lastBuildStatus.equals("SUCCESS") && currentBuild.result.equals('SUCCESS')){
        // changed from FAILURE or UNSTABLE OR Aborted to  SUCCESS
        buildChangeStatus = 'has recovered'
    }
  }
/*
  if("SUCCESS".equals(currentBuild.rawBuild.getPreviousBuild()?.getResult())) {
    echo "last build success"
  }else {
    echo "last build "+currentBuild.rawBuild.getPreviousBuild()?.getResult()
  }
  */
  // generate bitmap of recipients for selective email list generation
  def culpritId         =  1
  def developerId       =  2
  def requesterId       =  4
  def testSuspectId     =  8
  def buildSuspectId    = 16
  def recipientBitMap = developerId +
						testSuspectId +
						buildSuspectId

  // full recipients list bitmap
  // def recipientBitMap = culpritId +
                        // developerId +
						// requesterId  + 
						// testSuspectId +
						// buildSuspectId
						
	// Default values
  def colorName = 'RED'
  def colorCode = '#FF0000'
  def subject = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' ${buildChangeStatus}"
  def requester = manager.build.getEnvironment(manager.listener).get('BUILD_USER_ID')
  def summary = "${subject} (${env.BUILD_URL})"

  def details = """<p>${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
    <p>Check console output at "<a href="${env.BUILD_URL}">${env.JOB_NAME} [${env.BUILD_NUMBER}]</a>"</p>"""
 
  // Override default values based on build status
  if (buildStatus == 'STARTED') {
    color = 'YELLOW'
    colorCode = '#FFFF00'
  } else if (buildStatus == 'SUCCESSFUL') {
    color = 'GREEN'
    colorCode = '#00FF00'
  } else {
    color = 'RED'
    colorCode = '#FF0000'
  }
  
  def toCulprit      = emailextrecipients( [ [$class: 'CulpritsRecipientProvider'                 ] ])
  def toDevelop      = emailextrecipients( [ [$class: 'DevelopersRecipientProvider'               ] ])
  def toRequest      = emailextrecipients( [ [$class: 'RequesterRecipientProvider'                ] ])
  def toTestSuspect  = emailextrecipients( [ [$class: 'FailingTestSuspectsRecipientProvider'      ] ])
  def toBuildSuspect = emailextrecipients( [ [$class: 'FirstFailingBuildSuspectsRecipientProvider'] ])
  if ( "${env.gitlabUserEmail}" != 0){
    echo "gitlabUserEmail: " + env.gitlabUserEmail
    toRequest += env.gitlabUserEmail 
  }
  
  echo '[INFO] requester:                    (email notification temporarily disabled): \"' + requester + '\"'
  echo '[INFO] recipientProviders toCulprit: (email notification temporarily disabled): \"' + toCulprit + '\"'
  echo "[INFO] recipientProviders toDevelop: (email notification temporarily disabled): \"${toDevelop}\""
  echo '[INFO] send mail to:'
  echo "[INFO] recipientProviders toRequest:                                            \"${toRequest}\""
  echo "[INFO] recipientProviders toTestSuspect:                                        \"${toTestSuspect}\""
  echo "[INFO] recipientProviders toBuildSuspect:                                       \"${toBuildSuspect}\""

  // generate recipient list based on recipients enabled in bitmap
  def recipientList = ""
  if ( ( recipientBitMap & culpritId)      && ( toCulprit      != 0 ) ) {
	echo "[INFO] adding toCulprit"
  	recipientList += toCulprit
  }
  if ( ( recipientBitMap & developerId)    && ( toRequest      != 0 ) ) {
	echo "[INFO] adding toRequest"
  	recipientList +=  ";" + toRequest
  }
  if ( ( recipientBitMap & requesterId)    && ( requester      != 0 ) ) {
  echo "[INFO] adding requester"
  	recipientList += ";" + requester
  }
  if ( ( recipientBitMap & testSuspectId)  && ( toTestSuspect  != 0 ) ) {
  	echo "[INFO] adding toTestSuspect: " + toTestSuspect
	recipientList += ";" + toTestSuspect
  }
  if ( ( recipientBitMap & buildSuspectId) && ( toBuildSuspect != 0 ) ) {
  echo "[INFO] adding toBuildSuspect"
  	recipientList +=  ";" + toBuildSuspect
  }
  
  echo "[INFO] recipientList: " + recipientList
  
  
  // Send notifications
  emailext (
      mimeType: 'text/html',
      replyTo: '$DEFAULT_REPLYTO', 
      subject: subject,
      body: details,
      to: recipientList
  )

  if (env.gitlabMergeRequestId) {
  // this is a merge request send notification to gitlab
	//def resultIcon = currentBuild.result == 'SUCCESS' ? ':white_check_mark:' : ':anguished:'
	//addGitLabMRComment comment: "$resultIcon Jenkins Build $buildStatus\n\nResults available at: [Jenkins [$env.JOB_NAME#$env.BUILD_NUMBER]]($env.BUILD_URL)"
	//addGitLabMRComment comment: buildStatus == 'SUCCESS' ? ':+1:' : ':-1:'

	updateGitlabCommitStatus name: "$env.JOB_NAME", state: buildStatus == 'SUCCESS' ? 'success' : 'failed'
  }
}

/*
return [
	notificationHandler: this.&notifyBuild
	]
*/
return this;