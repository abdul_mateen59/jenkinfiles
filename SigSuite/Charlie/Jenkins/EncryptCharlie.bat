::encrypt Charly

set assemblyName=DATAmanager.exe
set sourceDir=%WORKSPACE%\\Charlie\\Sources\\bin\AnyCPU\\NightlyRelease
set sourceFile=%sourceDir%\\%assemblyName%
set targetDir=%sourceDir%\Encryption
set targetFile=%targetDir%\\%assemblyName%
set axPath=C:\\Program Files (x86)\\WIBU-SYSTEMS\AxProtector\\Devkit\\bin
set xmlFileTarget=AxProtectorSettings.xml
set xmlFileSource=p:\\sw_entw\\C_sharp\\Charlie\\CodeMeter\\Charlie-Codemeter-Ax.xml
set psFile=Charlie-Codemeter-Ax.ps1


if not exist %targetDir% md %targetDir%

:: xml-Datei erstellen mittels PowerShell

echo $lines = Get-Content "%xmlFileSource%" > "%psFile%"
echo [string]$targetFile = "%targetFile%" >> "%psFile%"
echo [string]$logFile = "%targetDir%\\CodemeterLog.log" >> "%psFile%"
echo [string]$sourceFile = "%sourceFile%" >> "%psFile%"
echo $co=0 >> "%psFile%"
echo $lines = Get-Content "%xmlFileSource%" > "%psFile%"
echo [string]$targetFile = "%targetFile%" >> "%psFile%"
echo [string]$logFile = "%targetDir%\\CodemeterLog.log" >> "%psFile%"
echo [string]$sourceFile = "%sourceFile%" >> "%psFile%"
echo $co=0 >> "%psFile%"
echo foreach($line in $lines){ >> "%psFile%"
echo 	$l = $line.ToLower() >> "%psFile%"
echo 	$i1 = $l.IndexOf("<command>") >> "%psFile%"
echo 	$i2 = $l.IndexOf("</command>") >> "%psFile%"
echo 	if ($i1 -ge 0 -and $i2 -gt $i1){ >> "%psFile%"
echo 		$cmd = $line.Substring($i1+"<command>".Length,$i2-$i1-"<command>".Length) >> "%psFile%"
echo 		$t = $cmd.Trim().ToLower()  >> "%psFile%"
echo 		if ($t.StartsWith("-rd:")){ >> "%psFile%"
echo                    $LocaleEN = New-Object System.Globalization.CultureInfo("en-US") >> "%psFile%"
echo 			$date = (Get-Date).tostring("yyyyMMMdd",$LocaleEN) >> "%psFile%"
echo 			$l = $line.Replace($cmd,"-rd:"+$date+",00:00:00") >> "%psFile%"
echo 		} >> "%psFile%"
echo 		elseif ($t.StartsWith("-o:")){ $l = $line.Replace($cmd,"-o:"+$targetFile) } >> "%psFile%"
echo 		elseif ($t.StartsWith("-#:")){ $l = $line.Replace($cmd,"-#:"+$logFile)  } >> "%psFile%"
echo 		elseif ($t.StartsWith("-p")) { $l = $line.Replace($cmd,"-p14")  } >> "%psFile%"
echo 		elseif (!$t.StartsWith("-")) { $l = $line.Replace($cmd,$sourceFile)  } >> "%psFile%"
echo 	} >> "%psFile%"
echo 	elseif ($l.Contains("<productcode>")) { >> "%psFile%"
echo 		$l = $line.Replace("13","14") >> "%psFile%"
echo 	} >> "%psFile%"
echo 	else { >> "%psFile%"
echo 		$l = $line >> "%psFile%"
echo 	} >> "%psFile%"
echo 	Write-Output $l >> "%psFile%"
echo 	$co++ >> "%psFile%"
echo } >> "%psFile%"
	
powershell -ExecutionPolicy Bypass -File %psFile% > %xmlFileTarget%

if exist "%axPath%\\AxProtectorNet4.exe" (
 "%axPath%\\AxProtectorNet4.exe" @"%xmlFileTarget%"
echo net4 gefunden
) else (
"%axPath%\\AxProtectorNet.exe" @"%xmlFileTarget%"
)
