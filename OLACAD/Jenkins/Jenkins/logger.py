import logging
import logging.handlers
#################################################################
#
#
# PLEASE add documentation, what's going on here
#
#################################################################
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)

# create console handler to output logger to console
ch = logging.StreamHandler()
ch.setLevel(logging.NOTSET)

# create formatter and add it to the handlers
formatter = logging.Formatter('%(name)s %(levelname)s %(message)s')
ch.setFormatter(formatter)

# add the handlers to logger
log.addHandler(ch)


## create a more eye-catching log entry
#
# @param logString  the string to log is expected as list to support multi-line comments
# @param logLevel   [info, warn, error] are currently supported
def logWithFrame(logString, logLevel='info'):
    logOptions = { 'info'   : log.info,
                  'warn'    : log.warn,
                  'error'   : log.error}

    logOptions[logLevel] ('********************************************************************************')
    for line in logString:
        logOptions[logLevel] ('*   '+ line)
    logOptions[logLevel] ('********************************************************************************')


def getFormattedLogString(logStringTuple):
    if len(logStringTuple) == 1:
        return logStringTuple[0]
    else:
        return ' '.join(logStringTuple)


def logInfo(*logString):
    log.info(getFormattedLogString(logString))

    
def logError(*logString):
    log.error(getFormattedLogString(logString))
