#!/bin/bash

#set -e

KMI_ROOT_DIR=/opt/tomcat/webapps/ROOT/

################################
#
# deploy and restart tomcat
#
################################
echo "Stoppe nun den Tomcat, sodass das KMI dort hin verschoben werden kann"
cd /opt/tomcat
#echo "Befinde mich nun in Verzeichnis: "
#echo $(dirname "$(readlink -f '$0')")


echo "shutdown KMI..."
if [ -z $(ps -ef |grep opt/tomcat |grep catalina|awk '{print $2}') ]; then
	echo "[info] no running tomcat found ... continue"
else
	echo "[INFO] found running Tomcat instance"
	sudo ./bin/shutdown.sh
	SLEEP_TIME=10
	# wait for server to shutdown
	for (( i=0; i<$SLEEP_TIME; i++ ))
	  do
		echo -e ". \c"
		sleep 1
	done
fi

echo "Nun wechsle in das Verzeichnis des Tomcats, um die bisherige KMI Version zu löschen!"

if [ -d "$KMI_ROOT_DIR" ]; then
    cd webapps/ROOT/
    rm -f -r *
else
    mkdir webapps/ROOT/
    cd webapps/ROOT/
fi

echo "Befinde mich nun in Verzeichnis: "
echo $(dirname "$(readlink -f '$0')")
echo "Löschen durchgeführt, nun packe die neue Version in den Tomcat!"

#find $WORKSPACE/target/ -name "kmi-production-*" | while read datei; do
for datei in $(find $WORKSPACE/ -name "kmi-production-*"); do
    echo "copy $datei ==> ./ROOT.war"
    cp $datei ./ROOT.war
done

echo "Version kopiert, nun entpacke sie..."
unzip -qq ROOT.war
#SKE: warum?
# mv ROOT.war ..

echo "KMI entpackt, nun starte die Kiste."
cd ../../

#echo "Vorherige BUILD_ID aus Variable: $BUILD_ID"
#VORHERIGE_BUILD_ID=$BUILD_ID
#echo "Vorherige BUILD_ID aus neuer Variable: $VORHERIGE_BUILD_ID"
#BUILD_ID=
sudo ./bin/startup.sh

SLEEP_TIME=5
# wait for server to start
for (( i=0; i<$SLEEP_TIME; i++ ))
  do
    echo -e ". \c"
    sleep 1
done

if [ -z $(ps -ef |grep opt/tomcat |grep catalina|awk '{print $2}') ]; then
	echo "[ERROR] failed to start Tomcat"
	exit 1
else
	echo "[INFO] Tomcat running"
	echo "KMI gestartet, es kann genutzt werden!"
fi
