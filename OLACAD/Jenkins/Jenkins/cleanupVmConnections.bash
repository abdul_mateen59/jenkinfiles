#!/bin/bash

##############################################
#
# clean up samba connection from Virtual machine
#
##############################################

GUEST_VM=""

echo "search for $1"

file="$1"
vmname=$(cat "$file")
echo "Provided VM $vmname"

for VM in `VBoxManage list runningvms | cut -f1 -d" " ` ; do
  echo "found running $VM"
  if [ $VM == \"$vmname\" ]; then
    # running VM found
    echo "set GUEST_VM=$vmname"
    GUEST_VM=$vmname
    break
  fi
done

if [ -z $GUEST_VM ]; then
  echo "INFO: VM [$1] not running"
  exit 0
fi

# unmount all shares in given VM
NUM_SHARES=`mount |grep $GUEST_VM | cut -f3 -d" " |wc -l`
echo "umount $NUM_SHARES share(s) from $GUEST_VM"

if [[ `mount |grep $GUEST_VM | cut -f3 -d" "|wc -l ` -gt 0 ]]; then
  for mountPoint in `mount |grep $GUEST_VM | cut -f3 -d" " `; do
    umount -l -v $mountPoint
    echo "FINISHED"
  done
fi
