#!groovy

//////////////////////////////////////////////////////////////////
//
// library for scm handling functions
//
//////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////
//
// function to handle checkout supporting merge requests in pipeline
// originally taken from: 
// https://vetlugin.wordpress.com/2017/01/31/guide-jenkins-pipeline-merge-requests/
//
//////////////////////////////////////////////////////////////////
def handleCheckout() {
	if (env.gitlabMergeRequestId) {
		echo 'Merge request detected. Merging...'
		def credentialsId = scm.userRemoteConfigs[0].credentialsId
		checkout ([ $class: 'GitSCM',
					branches: [[name: "${env.gitlabSourceNamespace}/${env.gitlabSourceBranch}"]],
					extensions: [
						[$class: 'PruneStaleBranch'],
						[$class: 'CleanCheckout'],
						[$class: 'CheckoutOption', timeout: 14], 
						[$class: 'PreBuildMerge',
							options: [
							fastForwardMode: 'NO_FF',
							timeout: 15,
							mergeRemote: env.gitlabTargetNamespace,
							mergeTarget: env.gitlabTargetBranch
							]
						]
					],
					userRemoteConfigs: [
//						[credentialsId: 'jenkins_2019-01-29', url: 'git@gitlab:SWE2/OLAcad.git']]
//						[credentialsId: credentialsId,
						[credentialsId: 'jenkins_2019-01-29',
						 name: env.gitlabTargetNamespace,
						 url: env.gitlabTargetRepoSshURL
						],
//						[credentialsId: 'jenkins_2019-01-29', url: 'git@gitlab:SWE2/OLAcad.git']]
//						[credentialsId: credentialsId,
						[credentialsId: 'jenkins_2019-01-29',
						 name: env.gitlabSourceNamespace,
						 url: env.gitlabTargetRepoSshURL
						]
					]
		])
	} else {
		echo 'No merge request detected. Checking out selected branch or develop by default'
		if (env.GIT_BRANCH_SELECTION) {
            checkout 	changelog: true, 
						poll: false, 
						scm: [	$class: 'GitSCM', 
								branches: [[name: env.GIT_BRANCH_SELECTION]],
								doGenerateSubmoduleConfigurations: false, 
								extensions: [[$class: 'CloneOption', depth: 0, noTags: false, reference: '', shallow: false, timeout: 13], 
											 [$class: 'CleanCheckout'],
											 [$class: 'CheckoutOption', timeout: 20], 
								 			 [$class: 'PruneStaleBranch']
											],
								submoduleCfg: [], 
								userRemoteConfigs: [[credentialsId: 'jenkins_2019-01-29', url: 'git@gitlab:SWE2/OLAcad.git']]
							]
		} else {
		    // original from this build
            checkout 	changelog: true, 
						poll: false, 
						scm: [	$class: 'GitSCM', 
								branches: [[name: '*/develop']],
								doGenerateSubmoduleConfigurations: false, 
								extensions: [[$class: 'CloneOption', depth: 0, noTags: false, reference: '', shallow: false, timeout: 13], 
											 [$class: 'CleanCheckout'],
											 [$class: 'CheckoutOption', timeout: 20], 
								 			 [$class: 'PruneStaleBranch']
											],
								submoduleCfg: [], 
								userRemoteConfigs: [[credentialsId: 'jenkins_2019-01-29', url: 'git@gitlab:SWE2/OLAcad.git']]
							]
		}
	}
}
/*
return [
	handleCheckout: this.&handleCheckout
	]
	*/
return this;