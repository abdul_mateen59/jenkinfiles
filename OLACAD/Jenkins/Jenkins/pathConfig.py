import os

"""
Intent of this file is to concentrate path definitions in one place.
Some of the directory and file names used for the unit test runs have to be
the same on the host and guest system (except for the root path).
The idea is to define the common part of a path name in a single variable
and then use this variable to define the full path on host and guest
system seperately.
"""

testDwgDir                 = 'testdwg'
olacadDir                  = 'OLACAD'
executeAcadUnitTestOnGuest = 'guestExecuteAcadUnitTest.py'
executeMsiFileonGuest      = 'guestExecuteMsiFile.py'
configFile                 = 'pathConfig.py'
#testResultZip              = 'testResult.zip'
#testResultTar              = 'testResult.tar'
feature                    = 'Normal'
OlacadInstallFileBaseName  = 'OlacadClientInstall_AutoCAD'
MsiResult                  = 'MsiExeResult.xml'

########################
acadToObjectArxVersion = {'2015': '2015', '2016': '2015', '2017': '2017', '2018': '2018', '2019': '2019', '2020': '2019'}
acadToInstallerLanguage = {'de-DE': 'de', 'en-US': 'en'}





#############################
# paths on linux host
#############################
# paths in workspace  unitTestBundlePath gs    AcadUnitTest bundle uTestFile  uTestFile scriptSourcePath
class WorkspacePath: 
    def __init__(self, acadVersion, feature = 'full', acadLanguage = 'de-DE'):
        testResultTar               = 'testResult_' + acadVersion + '.tar'
        self.feature                = feature
        self.acadVersion            = acadVersion
        self.workspace              = os.environ['WORKSPACE']
        self.unitTestRootPath       = os.path.join(self.workspace, 'AcadUnitTest')
        self.unitTestBinPath        = os.path.join(self.unitTestRootPath, 'bin', 'Debug_' + acadToObjectArxVersion[acadVersion])
        self.testDrawingsSourcePath = os.path.join(self.unitTestRootPath, testDwgDir)
        self.olacadSourcePath       = os.path.join(self.workspace, olacadDir)

        self.unitTestBundlePackage  = os.path.join(self.unitTestRootPath, 'PackageContents.xml')
        self.unitTestFiles          = [os.path.join(self.unitTestBinPath, 'AcadUnitTest.arx'), os.path.join(self.unitTestBinPath, 'AcadUnitTest.pdb')]

        self.scriptSourcePath       = os.path.join(self.workspace, 'scripts')
        self.scriptFiles            = [configFile, executeMsiFileonGuest, executeAcadUnitTestOnGuest]
        # full path to msi file on host
        if self.feature == 'Lite':
            self.OlacadInstallFileName  = os.path.join(self.workspace, OlacadInstallFileBaseName + acadToObjectArxVersion[acadVersion] + acadToInstallerLanguage[acadLanguage] + '_lite.msi')
        else:
            self.OlacadInstallFileName  = os.path.join(self.workspace, OlacadInstallFileBaseName + acadToObjectArxVersion[acadVersion] + acadToInstallerLanguage[acadLanguage]  + '_full.msi')
        self.MsiTestResult          = os.path.join(self.workspace, MsiResult)
        #self.testResultZip          = os.path.join(self.workspace, testResultZip) #unittest only
        self.testResultTar          = os.path.join(self.workspace, testResultTar) #unittest only

        print(acadToObjectArxVersion[acadVersion])
##############################
# paths on virtual machine
##############################
class VmPath:    
    
    @staticmethod
    def join(*paths):
        """Join for windows file system"""
        return '\\'.join(paths)
            
    def __init__(self, acadVersion, buildWorkspacePath = '', feature = 'full', acadLanguage = 'de-DE' ):
        testResultTar           = 'testResult_' + acadVersion + '.tar'    
        #join = lambda path, *paths: '\\'.join([path, *paths]) # join for Windows
        self.acadVersion        = acadVersion
        self.feature            = feature
        self.objectArxVersion   = acadToObjectArxVersion[acadVersion]
        self.pythonExe          = VmPath.join('C:', 'Python36', 'python.exe')
        self.acadExe            = VmPath.join('C:', 'Program Files', 'Autodesk', 'AutoCAD ' + self.acadVersion, 'acad.exe')
        self.killExe            = VmPath.join('C:', 'Windows', 'system32', 'taskkill.exe')
        self.unitTestBundlePath = VmPath.join('C:', 'ProgramData', 'Autodesk', 'ApplicationPlugins', 'AcadUnitTest.bundle')
        self.unitTestBinPath    = VmPath.join(self.unitTestBundlePath, 'bin', self.objectArxVersion)
        
        self.buildWorkspacePath = buildWorkspacePath
        self.olacadRootDir      = VmPath.join(buildWorkspacePath, olacadDir)

        if self.feature == 'Lite':
            self.assemblyFilesPath = VmPath.join(self.olacadRootDir, 'baugruppen', 'lite')
        else:
            self.assemblyFilesPath = VmPath.join(self.olacadRootDir, 'baugruppen', 'normal')

        # additional '' for trailing backslash
        self.testDrawingsPath    = VmPath.join('D:', testDwgDir, '')
        self.testResultPath      = 'D:\\olacad_test\\'
        self.scriptPath          = 'D:\\'
        self.testExecScriptPath  = VmPath.join(self.scriptPath, executeAcadUnitTestOnGuest)
        self.msiScriptonGuest    = VmPath.join(self.scriptPath, executeMsiFileonGuest)
        #self.testResultZip       = VmPath.join('D:', testResultZip)
        self.testResultTar       = VmPath.join('D:', testResultTar)
        self.workspaceOnGuest    = 'D:'
        # full path to msi file on guest
        if self.feature == 'Lite':
            self.OlacadMsiFile = VmPath.join(self.workspaceOnGuest, OlacadInstallFileBaseName + self.objectArxVersion + acadToInstallerLanguage[acadLanguage] + '_lite.msi')
        else:
            self.OlacadMsiFile = VmPath.join(self.workspaceOnGuest, OlacadInstallFileBaseName + self.objectArxVersion + acadToInstallerLanguage[acadLanguage] + '_full.msi')
        
        self.MsiTestResult       = VmPath.join('D:', MsiResult)
