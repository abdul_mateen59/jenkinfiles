#!/bin/bash

##############################################
#
# helper script to run autocadunittests
# in jenkins pipeline
#
##############################################

#export ACAD_VERSION="2018"
#export ACAD_LANGUAGE="de-DE"
#export VM_NAME="Win7de64_ACAD2018"

while getopts ':a:l:v:' OPTION ; do
  case "$OPTION" in
    a)   echo "setting ACAD_VERSION to ${OPTARG}"
		 export ACAD_VERSION=$OPTARG
		 ;;
    l)   echo "setting ACAD_LANGUAGE to $OPTARG"
		 export ACAD_LANGUAGE=$OPTARG
		 ;;
	v)   VM_OS=$OPTARG
		 echo "setting VM_OS to ${VM_OS}"
		 ;;
    *)   echo "Unknown parameter: ${OPTION}"
		 echo "usage:"
		 echo "$0 -a [ACAD_VERSION] - l [ACAD_LANGUAGE] -v [VM_OS]"
		 echo '$0 -a "2018" - l "de-DE" -v "Win7"'
  esac
done

#export VM_NAME=${VM_OS}${ACAD_LANGUAGE:0:2}64_ACAD${ACAD_VERSION}
export VM_NAME=${VM_OS}de64_ACAD${ACAD_VERSION}
echo "setting VM_NAME to ${VM_NAME}"

if ./runAcadCoverage.bash; then
    # success
	# convert path to match workspace location in this jenkins job
	pushd .
	cd ${WORKSPACE}
	# ? fix path 
	sed -i -e 's/jenkins\\workspace\\olacadbuild_2018/./g' AcadUnitTestCoverage.xml

	# convert original source directories and files to lowercase 
    # necessary for matching files in coverage report
	./scripts/convert2LowerCase.bash OLACAD TINCLUDE
	popd
	exit 0
else
    # error
  	echo '[ERROR] runCoverage.bash returned with failure'
	exit 1
fi

