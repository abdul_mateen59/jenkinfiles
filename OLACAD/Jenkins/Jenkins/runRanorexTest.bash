#!/bin/bash -xe

##############################################
#
# test script for executing Ranorex
# test runs
#
# !!! curently not maintained !!!
#
##############################################


# start VM
echo "start VM"
VBoxManage startvm   "Win7de64_ACAD2015RanorexTest"
sleep 5

useSmbShare=1

if [ $DEBUG_ENABLED ]; then 
	echo "DEBUG mode:    $DEBUG_ENABLED"
	JUNIT_OUT_DIR="D:\OLAcad"
	WORKSPACE=/home/local/ISV/jenkins/workspace/OLACADrun_RanorexTest
fi

echo "JUNIT_OUT_DIR: $JUNIT_OUT_DIR"
echo "Workspace:     $WORKSPACE"

#copy installation files to guest
if [[ "$useSmbShare" -eq 1 ]]; then
  echo "mount win shares..."
  mount /mnt/datastore/Jenkins/Win7de64_Ranorex_C
  mount /mnt/datastore/Jenkins/Win7de64_Ranorex_D
  echo "done"

  echo "copy installation files useSmbShare"
#!  cp $WORKSPACE/OlacadClientInstall_AutoCAD2015_de_full_x64.msi /mnt/datastore/Jenkins/Win7de64_Ranorex_D/OLAcad
  cp /mnt/datastore/Jenkins/VBoxXchange/RR_QandA.exe   /mnt/datastore/Jenkins/Win7de64_Ranorex_D/OLAcad
  cp /mnt/datastore/Jenkins/VBoxXchange/RR_QandA.rxtst /mnt/datastore/Jenkins/Win7de64_Ranorex_D/OLAcad
  
	# must already exist in guest. ACAD will REMOVE FROM SEARCH PATH if this directory is not existing
	##VBoxManage --nologo guestcontrol "Win7de64_ACAD2015RanorexTest" --username "Admin" --password "ac4tion!" mkdir "C:\ProgramData\Autodesk\ApplicationPlugins\AutocadUnitTestLoader.bundle"
#    cp /mnt/datastore/Jenkins/AutocadUnitTestLoader.bundle/autocad.lsp         /mnt/datastore/Jenkins/Win7de64_Ranorex_C/ProgramData/Autodesk/ApplicationPlugins/AutocadUnitTestLoader.bundle/
#    cp /mnt/datastore/Jenkins/AutocadUnitTestLoader.bundle/PackageContents.xml /mnt/datastore/Jenkins/Win7de64_Ranorex_C/ProgramData/Autodesk/ApplicationPlugins/AutocadUnitTestLoader.bundle/
#
#    mkdir /mnt/datastore/Jenkins/Win7de64_Ranorex_D/OLAcad/AutocadUnitTest.bundle
#    cp $WORKSPACE/AutocadUnitTest.bundle/AcadUnitTest.arx    /mnt/datastore/Jenkins/Win7de64_Ranorex_D/OLAcad/AutocadUnitTest.bundle/
#    cp $WORKSPACE/AutocadUnitTest.bundle/PackageContents.xml /mnt/datastore/Jenkins/Win7de64_Ranorex_D/OLAcad/AutocadUnitTest.bundle/
else
	echo "copy installation files use VBox copyto"
	VBoxManage --nologo guestcontrol "Win7de64_ACAD2015RanorexTest" --username "Admin" --password "ac4tion!" copyto --target-directory "d:\OLAcad" $WORKSPACE/OlacadClientInstall_AutoCAD2015_de_full_x64.msi

	## must already exist in guest. ACAD will REMOVE FROM SEARCH PATH if this directory is not existing
	##VBoxManage --nologo guestcontrol "Win7de64_ACAD2015RanorexTest" --username "Admin" --password "ac4tion!" mkdir "C:\ProgramData\Autodesk\ApplicationPlugins\AutocadUnitTestLoader.bundle"
	VBoxManage --nologo guestcontrol "Win7de64_ACAD2015RanorexTest" --username "Admin" --password "ac4tion!" copyto --target-directory "C:\ProgramData\Autodesk\ApplicationPlugins\AutocadUnitTestLoader.bundle\autocad.lsp" 				$WORKSPACE/AutocadUnitTestLoader.bundle/autocad.lsp
	VBoxManage --nologo guestcontrol "Win7de64_ACAD2015RanorexTest" --username "Admin" --password "ac4tion!" copyto --target-directory "C:\ProgramData\Autodesk\ApplicationPlugins\AutocadUnitTestLoader.bundle\PackageContents.xml" $WORKSPACE/AutocadUnitTestLoader.bundle/PackageContents.xml

	VBoxManage --nologo guestcontrol "Win7de64_ACAD2015RanorexTest" --username "Admin" --password "ac4tion!" mkdir "D:\OLAcad\AutocadUnitTest.bundle"
	VBoxManage --nologo guestcontrol "Win7de64_ACAD2015RanorexTest" --username "Admin" --password "ac4tion!" copyto --target-directory "D:\OLAcad\AutocadUnitTest.bundle/AcadUnitTest.arx"    $WORKSPACE/AutocadUnitTest.bundle/AcadUnitTest.arx
	VBoxManage --nologo guestcontrol "Win7de64_ACAD2015RanorexTest" --username "Admin" --password "ac4tion!" copyto --target-directory "D:\OLAcad\AutocadUnitTest.bundle/PackageContents.xml" $WORKSPACE/AutocadUnitTest.bundle/PackageContents.xml
fi

## OLAcad msi installieren:
echo "install OLAcad"
#! VBoxManage --nologo guestcontrol "Win7de64_ACAD2015RanorexTest" run --username "Admin" --password "ac4tion!" --wait-stdout --exe "C:\Windows\System32\msiexec.exe" -- "msiexec.exe" -package "D:\OLAcad\OlacadClientInstall_AutoCAD2015_de_full_x64.msi" -passive -qr -log "d:\OLACAD\install_msi_remote.log"

## if DEBUG Version is loaded also copy wupiEngineNet.dll
echo "copy WupiEngineNet.dll for testing debug arx"
if [[ "$useSmbShare" -eq 1 ]]; then
	cp $WORKSPACE/WupiEngineNet.dll	/mnt/datastore/Jenkins/Win7de64_Ranorex_C/ProgramData/Autodesk/ApplicationPlugins/OLACAD_Loader.bundle/
else
	VBoxManage --nologo guestcontrol "Win7de64_ACAD2015RanorexTest" copyto --username "Admin" --password "ac4tion!" --target-directory "C:\ProgramData\Autodesk\ApplicationPlugins\OLACAD_Loader.bundle/WupiEngineNet.dll" $WORKSPACE/WupiEngineNet.dll
fi

## start Ranorex test
echo "start ACAD and terminate after 600sec"
VBoxManage --nologo guestcontrol "Win7de64_ACAD2015RanorexTest" --username "Admin" --password "ac4tion!" run --exe "D:\OLAcad\RR_QandA.exe" --timeout 600000 -- "RR_QandA.exe" -ts:SKE_1_short_compare -zr -zrf:.RanorexReport.rxzlog

#echo "save junit_result file(s)"
if [[ "$useSmbShare" -eq 1 ]]; then
#	cp /mnt/datastore/Jenkins/Win7de64_Ranorex_D/OLAcad/junit_result.xml $WORKSPACE
	umount /mnt/datastore/Jenkins/Win7de64_Ranorex_C
	umount /mnt/datastore/Jenkins/Win7de64_Ranorex_D
else
	VBoxManage --nologo guestcontrol "Win7de64_ACAD2015RanorexTest" --username "Admin" --password "ac4tion!" copyfrom --verbose --target-directory "D:\AcadUnitTest\junit_result.xml" "D:\OLAcad\junit_result.xml"
fi

# poweroff and restore snapshot

echo "powering off and restore"
VBoxManage controlvm "Win7de64_ACAD2015RanorexTest" poweroff
VBoxManage snapshot  "Win7de64_ACAD2015RanorexTest" restorecurrent
