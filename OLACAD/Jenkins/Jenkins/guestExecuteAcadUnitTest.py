
#################################################################
#
#
# PLEASE add documentation, what's going on here
#
#################################################################

import os
import shutil
import subprocess as sp
import time
import shutil
#import zipfile
import tarfile
import logging

import sys

sys.path.append('D:\\') # module include path for virtual machine
from pathConfig import VmPath

unitTestTimeout     = 600
resultsPollInterval = 5
junitFile       = 'D:\\olacad_test\\junit_result.xml'
logFile         = 'D:\\testrun.log'
testResultFiles = [['cobertura', 'D:\\AcadUnitTestCoverage.xml'], ['html', 'D:\\CoverageReport'], ['binary', 'D:\\AcadUnitTestCoverage.cov']]


logging.basicConfig(filename=logFile,
                            filemode='a',
                            format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                            datefmt='%H:%M:%S',
                            level=logging.DEBUG)

logging.info("LOGGER running")

logger = logging.getLogger('PY_DEBUG')
#############################################################

def getTasks(name):
    r = os.popen('tasklist /v').read().strip().split('\n')
    for i in range(len(r)):
        s = r[i]
        if name in r[i]:
            #print ('%s in r[i]' %(name))
            return r[i]
    return []

def executeTests(acadLanguage, acadVersion, path):
    """Executes unit tests by starting OpenCppCoverage with AutoCad as parameter.
    Test results are polled for with 'resultsPollInterval'.
    Once test results have been written, AutoCad is killed.

    Args:
        acadLanugage (str): e.g. 'de-DE'
        acadVersion (str): AutoCad version to start, e.g. '2016'
        path (pathConfig.VmPath): path configuration on vm
    """
    excludeProjDir = os.path.join(path.buildWorkspacePath, "AcadUnitTest")
    excludeCatchDir = os.path.join(path.buildWorkspacePath, "olacadCatch")
    excludeLangDir = os.path.join(path.olacadRootDir, "Source", "language")
    
    logger.debug('configure command line')
    command=([r'c:\Program files\OpenCppCoverage\OpenCppCoverage.exe'])
    command.extend(['--continue_after_cpp_exception'])
    #command.extend(['--working_dir=d:\\']) # SKE: not needed???
    #command.extend(['--verbose'])          # SKE: for debugging add full debug printout OpenCppCoverage
    
    command.extend(['--modules=AcadUnitTest.arx'])
    command.extend(['--sources=' + path.olacadRootDir])
    command.extend(['--excluded_sources=' + excludeProjDir])
    command.extend(['--excluded_sources=' + excludeCatchDir])
    command.extend(['--excluded_sources=' + excludeLangDir])
    for resultFile in testResultFiles:
        command.extend(['--export_type=' + resultFile[0] + ':' + resultFile[1]])
    
    command.extend(['--', path.acadExe, '-product', 'ACAD',  '-language', acadLanguage])
    print(command)
    logger.debug('execute:' + str(command) )
    
    try:
        process = sp.Popen(command, cwd='d:\\')
        logger.debug('running...')
        print('[INFO] OpenCppCoverage launched')

        passedTime = 0
        while passedTime < unitTestTimeout:
            time.sleep(resultsPollInterval)
            passedTime += resultsPollInterval
            logger.debug('check 4 result [ ' + str(passedTime) + 'sec]')
            if os.path.isfile(junitFile) and os.path.getsize(junitFile) > 0:
                print("\nJunit file was written.")
                print("\nJunit file size:", os.path.getsize(junitFile))
                logger.debug("Junit file was written.")
                break

        if (passedTime >= unitTestTimeout):
            print("\nTimeout reached before test results were written")
            logger.debug("Timeout reached before test results were written")
        
        runningTasks = getTasks('acad.exe')
        if runningTasks:
            print("Terminating still running AutoCad now\n")
            logger.debug("Terminating still running AutoCad now")
            sp.call([path.killExe, '/F', '/IM',  'acad.exe'])
        
        print("\nWaiting for coverage to finish\n")
        logger.debug("Waiting for coverage to finish")

        process.wait()                  

        logger.debug("OpenCppCoverage finished")
    except:
        logger.logError(sys.exc_info()[0])
    finally:
        logger.debug("executeTests() finished")

        
def setUpTestEnvironment(path):
    """sets up the environment for test execution, 
    for now: creates symbolic link in the AcadUnitTest.bundle folder to assembly files.

    Args:
        path (VmPath): path configuration for vm
    """
    
    sp.call(['mklink', '/J', os.path.join(path.unitTestBundlePath, 'baugruppen'), path.assemblyFilesPath], shell=True)
        
def zipTestResults():
    # shutil -> simple way to create the zip archive for the directory tree of the report
    ## this is not working reliable ! we use tar format instead - see below
    #zipname = shutil.make_archive('d:\\testResult', 'zip', 'D:\\CoverageReport', 'D:\\CoverageReport')
    # append separate files with zipfile module
    #testResultZip = zipfile.ZipFile(zipname, 'a')
    #testResultZip.write('D:\\AcadUnitTestCoverage.xml')
    #testResultZip.write('D:\\AcadUnitTestCoverage.cov')
    #testResultZip.write('D:\\LastCoverageResults.log')
    #testResultZip.write(junitFile, os.path.basename(junitFile))
    #testResultZip.write(logFile)
    #testResultZip.close()

    tarname = shutil.make_archive('d:\\testResult_' + sys.argv[2] , 'tar' , 'D:\\CoverageReport', 'D:\\CoverageReport')
    testResultTar = tarfile.TarFile(tarname, 'a')
    testResultTar.add('D:\\AcadUnitTestCoverage.xml')
    testResultTar.add('D:\\AcadUnitTestCoverage.cov')
    testResultTar.add('D:\\LastCoverageResults.log')
    testResultTar.add(junitFile)
    testResultTar.add(logFile)
    testResultTar.close()


if __name__ == '__main__':

    print('Using', os.environ['TESTDWG_DIR'], 'to look for test dwg files')
    print('Using', os.environ['JUNIT_OUT_DIR'], 'as target directory for junit result file')

    # default parameters
    acadLanguage = 'de-DE'
    acadVersion = '2018'
    buildWorkspace = 'D:\\jenkins\\workspace\\OLACADbuild'
    feature = 'Normal'
    if len(sys.argv) == 5:
        acadLanguage = sys.argv[1]
        acadVersion = sys.argv[2]
        buildWorkspace = sys.argv[3]
        feature = sys.argv[4]
        
    path = VmPath(acadVersion, buildWorkspace, feature)

    setUpTestEnvironment(path)
    executeTests(acadLanguage, acadVersion, path)
    zipTestResults()
    
