import os
import subprocess as sp
import time
import sys
from junit_xml import TestSuite, TestCase

unitTestTimeout = 120
resultsPollInterval = 5
###error check
MsiResultFile = 'D:\\MsiExeResult.xml'
#################String of Installation Results#########################
result1='#Return Value of MSI Installation:#'
result2='#Installation Terminated#'
result3='#Installation finished#'
result4='#Installation failed#'

########################################################################    
#############     XML File For Error Situation      ####################
########################################################################
def testTerminationInfoToConsole():
    ''' 1 suite and test case with failure info added. Output to console.'''

    test_cases = TestCase('msitest', 'msi installer', 123.345, '', 'msiexec terminated')
    test_cases.add_error_info('Invalid File \'DNC.exe\'.')
    ts = [TestSuite("msitest", [test_cases])]
    # pretty printing is on by default but can be disabled using prettyprint=False
    with open(r'D:\\MsiExeResult.xml', mode='a') as lFile:
        TestSuite.to_file(lFile, ts, prettyprint=True)
        lFile.close()

########################################################################    
############# XML File For successfull Installation ####################
########################################################################
def testBasicToConsole():
    ''' Perform the very basic test with 1 suite and 1 test case, output to console.'''

    test_cases = [TestCase('msitest', 'msi installer', 1.14, '', '')]
    ts = [TestSuite("msitest", test_cases)]
    # pretty printing is on by default but can be disabled using prettyprint=False
    with open(r'D:\\MsiExeResult.xml', mode='a') as lFile:
        TestSuite.to_file(lFile, ts, prettyprint=True)
        lFile.close()

########################################################################    
############# XML File For failure situation ###########################
########################################################################
def testFailureInfoToConsole():
    ''' 1 suite and test case with failure info added. Output to console.'''

    test_cases = TestCase('msitest', 'msi installer', 123.345, '', 'msiexec failed')
    test_cases.add_failure_info('versions are not compatible \'DNC.exe\'.')
    ts = [TestSuite("msitest", [test_cases])]
    # pretty printing is on by default but can be disabled using prettyprint=False
    with open(r'D:\\MsiExeResult.xml', mode='a') as lFile:
        TestSuite.to_file(lFile, ts, prettyprint=True)
        lFile.close()


##############################################################################
##   Execute msi installation tests                                         ##
##############################################################################
def executeMsi( msiFile ):
    print("executeMsi: ",  msiFile)

    command=(['C:\Windows\System32\msiexec.exe'])
    command.extend(['/i'])
    command.extend([msiFile])
    command.extend(['/passive'])
    command.extend(['/qr'])
    command.extend(['/log'])
    command.extend(['d:\install_msi_remote.log'])
    print(command)

    try:
        retcode = sp.check_call(command)
        print(result1,'==> ',retcode,' <==') 
        if retcode < 0:
            print(result2)
            testTerminationInfoToConsole()
        else:
            print(result3)
            testBasicToConsole()
    except sp.CalledProcessError as e:
         print(result4,e)
         testFailureInfoToConsole()

    passedTime = 0
    while passedTime < unitTestTimeout:
        time.sleep(resultsPollInterval)
        passedTime += resultsPollInterval
        if os.path.isfile(MsiResultFile) and os.path.getsize(MsiResultFile) > 0:
            #time.sleep(2) ## assure Msi_result_file is written
            print("\nMSI-Test result file was written.")
            print("\nMSI-Test result file size:", os.path.getsize(MsiResultFile))
            logger.debug("MSI-Test result file was written.")
            break

    if (passedTime >= unitTestTimeout):
        print("\nTimeout reached before test results were written")
        logger.debug("Timeout reached before test results were written")


if __name__ == '__main__':
    if len(sys.argv) == 2:
        OlacadClientMsi = sys.argv[1]
    executeMsi(OlacadClientMsi)
