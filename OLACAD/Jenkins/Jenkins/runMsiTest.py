import os
import sys
import shutil
import subprocess as sp
import time
import zipfile
import logger
from datetime import datetime
import logger
import vmHandler

version = 'running on python version {0.major}.{0.minor}.{0.micro}'.format(sys.version_info)
logger.logInfo(version)

vmVersion = u'5.2.0'

if (sys.version_info < (3, 4)):
  logger.logInfo( "using pathlib2")
  import pathlib2 as pathlib
else:
  logger.logInfo( "using pathlib")
  import pathlib

from pathConfig import WorkspacePath, VmPath
#################################################################
#
#
# PLEASE add documentation, what's going on here
#
#################################################################
# credentials for vm guest session
username = 'admin'
password = 'ac4tion!'
def guaranteeDirectory(guestSession, directory):
    """Creates (recursively) the full path specified in directory if it does not exists.
    Example: if directory = "D:\\jenkins\\workspace\\OLACAD" and "D:\\jenkins" already exists 
    on the guest, "D:\\jenkins\\workspace" and "D:\\jenkins\\workspace\\OLACAD" will be
    created.
    """
    def isDriveLetter(directory):
        return len(directory) == 2 and directory[-1] == ':'
    
    if directory == '' or isDriveLetter(directory):
        return
    try:
        # if dir is not existing exception can be thrown !
        if not guestSession.directory_exists(directory):
            logger.logInfo('Creating', directory)
            guestSession.makedirs(directory)
            if guestSession.directory_exists(directory):
                return
            #guaranteeDirectory(guestSession, '\\'.join(directory.split('\\')[:-1])) # guarantee parent directory exists
            #guestSession.directory_create(directory, 1, [])
    except:
        logger.logInfo('Creating', directory)
        try:
            guestSession.makedirs(directory)
            if guestSession.directory_exists(directory):
                return
        except:
            guaranteeDirectory(guestSession, '\\'.join(directory.split('\\')[:-1])) # guarantee parent directory exists
            #guestSession.directory_create(directory, 1, [])


def copy_tree(guestSession, hostPath, guestPath, vmVersion):
    """Copies the directory tree starting at hostPath from the host machine
    to the directory guestPath on the guest machine.

    Args:
        gs (IGuestSession): guestSession used to copy files
        hostPath (WorkspacePath): path configuration on host machine
        guestPath (VmPath): path configuration on guest machine
    """

    for src_dir, dirs, files in os.walk(hostPath):
        dst_dir = src_dir.replace(hostPath, guestPath, 1)
        winTargetPath = pathlib.PureWindowsPath(dst_dir)
        guaranteeDirectory(guestSession, str(winTargetPath))
        for file_ in files:
            src_file = os.path.join(src_dir, file_)
            winTargetFile = winTargetPath / file_
            if vmVersion == u'5.2.8':
                p = guestSession.file_copy_to_guest(src_file, str(winTargetPath), [])
            else:
                p = guestSession.file_copy_to_guest(src_file, str(winTargetFile), [])
            p.wait_for_completion()

def deployFiles(gs, hostPath, guestPath, vmVersion):
    """Copies all files for test execution from host to guest.

    Args:
        gs (IGuestSession): guestSession used to copy files
        hostPath (WorkspacePath): path configuration on host machine
        guestPath (VmPath): path configuration on guest machine
    """
   
    logger.logWithFrame(['Copying files from host to virtual machine'])
      
    ### scripts for local execution of MSI file
    logger.logInfo('Copying test execution scripts')
    winTargetPath = pathlib.PureWindowsPath(guestPath.scriptPath)
    for scriptFile in hostPath.scriptFiles:
        sourceFile = os.path.join(hostPath.scriptSourcePath, scriptFile)
        winTargetFile = winTargetPath / scriptFile
        if vmVersion == u'5.2.8':
            p = gs.file_copy_to_guest(sourceFile, str(winTargetPath), [])
        else:
            p = gs.file_copy_to_guest(sourceFile, str(winTargetFile), [])
        logger.logInfo(p.description)
        p.wait_for_completion()
        if p.result_code != 0:            
            logger.logError('Error when copying', sourceFile)
            logger.logError(p.error_info.text)

####copying msi files#############
    logger.logInfo('Copying msi files')
    winTargetPath = pathlib.PureWindowsPath(guestPath.workspaceOnGuest)
    pu = pathlib.PurePath(hostPath.OlacadInstallFileName)
    winTargetFile = winTargetPath / pu.name
    if vmVersion == u'5.2.8':
        p = gs.file_copy_to_guest(hostPath.OlacadInstallFileName, str(winTargetPath), [])
    else:
        p = gs.file_copy_to_guest(hostPath.OlacadInstallFileName, str(winTargetFile), [])
    logger.logInfo(p.description)
    p.wait_for_completion()
    if p.result_code != 0:            
            logger.logError('Error when copying', hostPath.OlacadInstallFileName)
            logger.logError(p.error_info.text)
	
###################################

def startTest(gs, guestPath ):
    """Triggers test execution on the guest machine.
    Therefore the test execution script is executed on the guest via pyvbox api.
    All files necessary for test execution (including the test execution script)
    have to be present on the guest for this function to work.

    Args:
        gs (IGuestSession): guestSession used to run unit test script on vm        
        guestPath (VmPath): path configuration on guest machine
    """

    logger.logWithFrame(['Starting test execution script and msi_file on virtual machine'])

    environVars = {}
    environVars['JUNIT_OUT_DIR'] = guestPath.testResultPath
    environVars['TESTDWG_DIR'  ] = guestPath.testDrawingsPath
    
    envVarList = [var + '=' + environVars[var] for var in environVars]
    
    logger.logInfo('setting environment variables on guest')
    for var in envVarList:
        logger.logInfo(var)

    ### Using IGuestSession.execute for test execution we have to wait for the
    ### test execution script to finish in order to get stdout and stderr.
    ### Alternativly we could also start a process on the VM asyncronically.
    ### We could then probably flush stdout and stderr during the test execution.

    start_time = datetime.now()
    print('Msiexec Unit Test Start Time:',start_time)

    process, stdout, stderr = gs.execute(guestPath.pythonExe, [guestPath.msiScriptonGuest, guestPath.OlacadMsiFile], '', envVarList)

    #############
    end_time = datetime.now()
    print('Msiexec Unit Test End Time:',end_time)
    print('Timestamp: {}'.format(end_time - start_time))
    #############

    infoFromVm  = ['VM Log ' + line for line in stdout.split('\n') if 'skipped' not in line]
    errorFromVm = ['VM Log ' + line for line in stderr.split('\n') if 'skipped' not in line]

    for line in infoFromVm:
        logger.logInfo(line)

    for line in errorFromVm:
        logger.logError(line)

    logger.logInfo('test execution has finished, waiting for test results')

def gatherTestResults(gs, hostPath, guestPath):
    """Copies the zipped test results form guest to host
    and extracts the zip to the workspace.

    Args:
        gs (IGuestSession): guestSession used to copy files
        hostPath (WorkspacePath): path configuration on host machine
        guestPath (VmPath): path configuration on guest machine
    """

    logger.logWithFrame(['Gathering test results', 'copying files from virtual machine to host'])

    p = gs.file_copy_from_guest(guestPath.MsiTestResult, hostPath.MsiTestResult, [])
    logger.logInfo(p)
    p.wait_for_completion()
###############################################
#deletef function to start and stop the virtualmachine

def resolveCommandLineOptions(argv):
    """primitive method to map the command line arguments to variables"""

    # default parameters, used when number of command line options does not match the expected number (6)
    OSVersion          = 'Win7de'
    acadLanguage       = 'de-DE'
    acadVersion        = '2017'
    vmFrontend         = 'gui'
    resultDir          = 'D:'
    feature            = 'Normal'

    expectedArgCount   = 5
    if len(argv) == expectedArgCount:
            logger.logInfo('Using command line arguments:')
            OSVersion          = argv[0]
            acadLanguage       = argv[1]
            acadVersion        = argv[2]
            vmFrontend         = argv[3]
            feature            = argv[3]
    else:
        logger.logInfo('Expected number of arguments: ' + str(expectedArgCount))
        logger.logInfo('Supplied number of arguments: ' + str(len(argv)))
        logger.logInfo('Using default parameters:')
        
    vmName = OSVersion + '64_ACAD' + acadVersion
    
    logger.logInfo('OS Version:  ', OSVersion)    
    logger.logInfo('Acad language:  ', acadLanguage)
    logger.logInfo('Virtual Machine:', vmName)
    logger.logInfo('Front end:      ', vmFrontend)
    logger.logInfo('FEATURE:        ', feature)
    
    return vmName, vmFrontend, resultDir, acadVersion, acadLanguage, feature

def run(argv):
    """runs msi install tests on a virtual machine.
    Arguments have to be supplied in fixed order and number

    Args (all str):
        acadLanguage: e.g. 'de-DE'
        acadVersion: AutoCad version to use for running unit tests, e.g. '2016'
        vmFrontend: 'gui' or 'headless'
        feature: program feature 'Lite' or 'Normal'
    """

    logger.logWithFrame(['Running MsiInstallTest'])

    vmName, vmFrontend, resultDir, acadVersion, acadLanguage, feature  = resolveCommandLineOptions(argv)

    hostPath  = WorkspacePath(acadVersion, feature, acadLanguage)
    guestPath = VmPath(acadVersion, '', feature, acadLanguage)

    logger.logInfo('')
    logger.logInfo('Current jenkins workspace on host machine is', hostPath.workspace)

    vmstate, vmName = vmHandler.getFreeVm(vmName)
    try:
        f=open("VM_NAME.txt",'w+')
        f.write(vmName)
        logger.logInfo('VM_NAME File Created ' + str(vmName))
    except IOError:
        logger.logInfo("Unable to create file on disk.")
    finally:
        f.close()
    if vmstate == vmHandler.returnSaved:
        session, testMachine, vmStartState, vmVersion = vmHandler.startVm(vmName, vmFrontend)
    else:
        raise Exception("VM not available")

    logger.logInfo('Logging into virtual machine as user', username)
    guestSession = session.console.guest.create_session(username, password)  

    try:
        deployFiles(guestSession, hostPath, guestPath, vmVersion)

        startTest( guestSession, guestPath )

        time.sleep(4) # assure proper result file state in vm

        gatherTestResults(guestSession, hostPath, guestPath)

    except:
        logger.logError(sys.exc_info()[0])
    finally:
        logger.logWithFrame(['Logging out user', username])
        guestSession.close()

    logger.logWithFrame(['shutting down virtual machine and restoring snapshot'])
    vmHandler.restoreVmSnapshot(session, testMachine, vmStartState)
    logger.logWithFrame(['virtual machine down'])

    return session, guestSession, testMachine, vmStartState

if __name__ == '__main__':
    session, guestSession, vm, vmStartState = run(sys.argv[1:])
