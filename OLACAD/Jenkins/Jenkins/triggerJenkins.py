###!/usr/bin/python
########################################
#
# trigger jenkins job using python API
# start job and wait for start. 
# this functin DOES NOT WAIT FOR JOB COMPLETION
#
#  see also: 
#
# https://stackoverflow.com/questions/8384143/how-to-trigger-authenticated-jenkins-job-with-file-parameter-using-standard-pyth
# https://pypi.python.org/pypi/jenkinsapi
# https://python-jenkins.readthedocs.io/en/latest/index.html
########################################
#from jenkinsapi import jenkins
import jenkins
import os
import sys
import re
import getopt
import subprocess as sp
import time

def run(argv):
    jobName = ''
    jenkinsParams = {}

    ######################################
    # parse arguments                    #
    ######################################
    try:
        opts, args = getopt.getopt(argv,"",["job=","params="])
    except getopt.GetoptError:
        print ('usage: triggerJenkins --job <JobName> [--params="param_1=value param_2=value2 ... param_n=valuen"]')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '--job':
            jobName = arg
            print ('found argument job:' , jobName)
        elif opt in ("--params"):
            print ('found argument params:' , arg)
            jenkinsParams = dict(re.findall('(\S+)=(.*?)(?=\s\S+=|$)', arg))
            print (jenkinsParams)

    ######################################
    # build job params and trigger job   #
    ######################################
    try:
        jenkins_server_url = 'http://sigdrs109:8080'
        server  = jenkins.Jenkins(jenkins_server_url, username='jenkins', password='Achtung!')
        user    = server.get_whoami()
        job     = server.get_job_info(jobName)
        prev_id = job['lastBuild']['number']
#        print ('lastBuild: ' , prev_id)

        print( "User: ", user['fullName'])
#        print("Jenkins version: ", j.get_version() )
#        print ("jobs:",j.get_jobs())
    #    param1       = "VM_NAME"
    #    param1_value = "Win7de64_ACAD2015"
    #    param2       = "UPSTREAM_NODE"
    #    param2_value = "VMHOST_Lin-0"
        api_token    = '8c3913c059c7df6f9156906f1053663a'
        print ("start job:",    jobName)
        print ("with params: ", jenkinsParams)
#        print ("jobs:",j.get_jobs())
#        server.build_job(jobName,{'token': api_token}) #works
#        server.build_job(jobName, {param1: param1_value, param2 : param2_value}, {'token': api_token}) # does not work
        server.build_job(jobName, jenkinsParams)

        while True:
            print('Waiting for build to start...')
            if prev_id != server.get_job_info(jobName)['lastBuild']['number']:
                break
            time.sleep(3)
        print('Running...')
        
# todo: get build status = running using pythonjenkins instead jenkinsapi
#        last_build = job['lastBuild']
#        while last_build.is_running():
#            time.sleep(1)
#        print(str(last_build.get_status()))

        print ("fertsch")
    except:
        print(sys.exc_info()[0:])

    return 0


if __name__ == '__main__':
    retVal = run(sys.argv[1:])


## https://stackoverflow.com/questions/31721922/build-job-in-jenkins-through-python-script
##!/usr/bin/python
## coding: utf-8
#import jenkins
#ci_jenkins_url = "http://my-jenkins-url/"
#username = "foo"
#token = "fa818f4f90621a4e69de563516098689"
#job = "test-job"
#j = jenkins.Jenkins(ci_jenkins_url, username=username, password=token)
#
#if __name__ == "__main__":
#      j.build_job(job, {'token': token})